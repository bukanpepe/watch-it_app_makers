CREATE DATABASE "watch-it_app"
    WITH 
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'English_United States.1252'
    LC_CTYPE = 'English_United States.1252'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;

CREATE TABLE public."user"
(
    user_id serial NOT NULL,
    first_name character varying(255) NOT NULL,
	last_name character varying(255),
    email_address character varying(255) NOT NULL,
	password character varying(255) NOT NULL,
    user_address character varying(255),
    balance integer,
    CONSTRAINT user_pkey PRIMARY KEY (user_id)
)

TABLESPACE pg_default;

CREATE TABLE public.user_session
(
    user_id integer NOT NULL,
    user_token character varying(25) NOT NULL
)

TABLESPACE pg_default;

CREATE TABLE public.category
(
	category_id serial NOT NULL,
    cat_name character varying(20),
    CONSTRAINT category_pkey PRIMARY KEY (category_id)
)

TABLESPACE pg_default;

CREATE TABLE public.movie
(
    movie_id serial NOT NULL,
    title character varying(255) NOT NULL,
    description text,
    directed_by character varying(255),
	release_year int,
	duration int,
    show_status character varying(25) DEFAULT 'No Show',
    poster_img character varying(255),
    CONSTRAINT movie_pkey PRIMARY KEY (movie_id)
)

TABLESPACE pg_default;

CREATE TABLE public.movie_cat
(   
    mc_id serial NOT NULL,
    movie_id int NOT NULL,
	category_id int NOT NULL,
    CONSTRAINT pk_mc_id,
	CONSTRAINT fk_movie_id FOREIGN KEY (movie_id) REFERENCES movie (movie_id),
	CONSTRAINT fk_cat_id FOREIGN KEY (category_id) REFERENCES category (category_id)
)

TABLESPACE pg_default;

CREATE TABLE public.studio
(
    studio_id serial NOT NULL,
    capacity int NOT NULL,
    layout int[] NOT NULL,
    CONSTRAINT studio_pkey PRIMARY KEY (studio_id)
)

TABLESPACE pg_default;

CREATE TABLE public.day_schedule
(
    day_id serial NOT NULL,
    day_name character varying(20) NOT NULL,
	opening_hours time,
	closing_hours time,
    price integer,
    CONSTRAINT day_pkey PRIMARY KEY (day_id)
)

TABLESPACE pg_default;

CREATE TABLE public.schedule
(
    schedule_id serial NOT NULL,
    day_id int NOT NULL,
    play_date date NOT NULL,
    movie_id int NOT NULL,
	studio_id int NOT NULL,
	start_time time NOT NULL,
    CONSTRAINT schedule_pkey PRIMARY KEY (schedule_id),
    CONSTRAINT fk_day_id FOREIGN KEY (day_id) REFERENCES day_schedule (day_id),
	CONSTRAINT valid_date_day CHECK (extract(isodow from play_date) = day_id),
	CONSTRAINT fk_studio_id FOREIGN KEY (studio_id) REFERENCES studio (studio_id),
	CONSTRAINT fk_movie_id FOREIGN KEY (movie_id) REFERENCES movie (movie_id)
)

TABLESPACE pg_default;

CREATE TABLE public.ticket
(
    ticket_id serial NOT NULL,
    schedule_id int NOT NULL,
    user_id int NOT NULL,
    ticket_qty int NOT NULL,
    CONSTRAINT ticket_pkey PRIMARY KEY (ticket_id),
    CONSTRAINT fk_schedule_id FOREIGN KEY (schedule_id) REFERENCES schedule (schedule_id),
	CONSTRAINT fk_user_id FOREIGN KEY (user_id) REFERENCES public.user (user_id)
)

TABLESPACE pg_default;

CREATE TABLE public.seat_taken
(
    schedule_id integer NOT NULL,
    seat_id character varying(4) NOT NULL
)

TABLESPACE pg_default;

CREATE TABLE public.transaction
(
    trx_id serial NOT NULL,
    user_id int,
    top_up numeric,
    purchase numeric,
	trx_stamp timestamp NOT NULL,
    CONSTRAINT trx_pkey PRIMARY KEY (trx_id),
    CONSTRAINT fk_user_id FOREIGN KEY (user_id) REFERENCES public.user (user_id)
)

TABLESPACE pg_default;

CREATE FUNCTION valid_hours() RETURNS trigger AS $valid_hours$
    BEGIN
        IF NEW.day_id IS NULL THEN
            RAISE EXCEPTION 'day_id cannot be null';
        END IF;
        IF NEW.start_time IS NULL THEN
            RAISE EXCEPTION '% cannot have null start_time', NEW.day_id;
        END IF;

        IF NEW.start_time > (Select closing_hours from day_schedule, schedule where NEW.day_id = day_schedule.day_id)S
		THEN
            RAISE EXCEPTION '% cannot be after closing hours', NEW.day_id;
        END IF;
		RETURN NEW;
    END;
$valid_hours$ LANGUAGE plpgsql;

CREATE TRIGGER valid_hours BEFORE INSERT OR UPDATE ON schedule
    FOR EACH ROW EXECUTE PROCEDURE valid_hours();

CREATE FUNCTION public.trx_stamp()
    RETURNS trigger
    LANGUAGE 'plpgsql'
     NOT LEAKPROOF
AS $BODY$
BEGIN
	NEW.trx_stamp = CURRENT_TIMESTAMP;
	RETURN NEW;
END;
$BODY$;

ALTER FUNCTION public.trx_stamp()
    OWNER TO postgres;

CREATE TRIGGER trx_stamp
    BEFORE INSERT
    ON public.transaction
    FOR EACH ROW
    EXECUTE PROCEDURE public.trx_stamp();

                    --POPULATE DATABASE
--MOVIE TABLE
INSERT INTO movie
(title, description, directed_by, release_year, duration, show_status, poster_img)
VALUES 
    ('Wonder Woman 1984', 'Diana must contend with a work colleague and businessman, whose desire for extreme wealth sends the world down a path of destruction, after an ancient artifact that grants wishes goes missing.', 'Patty Jenkins', 2020, 151, 'Now Playing', 'https://m.media-amazon.com/images/M/MV5BNWY2NWE0NWEtZGUwMC00NWMwLTkyNzUtNmIxMmIyYzA0MjNiXkEyXkFqcGdeQXVyMTA2OTQ3MTUy._V1_UX182_CR0,0,182,268_AL_.jpg'),
    ('Demon Slayer the Movie: Mugen Train', 'Tanjiro Kamado, joined with Inosuke Hashibira, a boy raised by boars who wears a boar''s head, and Zenitsu Agatsuma, a scared boy who reveals his true power when he sleeps, board the Infinity Train on a new mission with the Flame Pillar, Kyojuro Rengoku, to defeat a demon who has been tormenting the people and killing the demon slayers who oppose it!', 'Haruo Sotozaki', 2021, 117, 'Coming Soon', 'https://m.media-amazon.com/images/M/MV5BNzY1MWExOWUtZTNjMC00MzY2LWIxYTYtNjZjNThhNDBjNTZlXkEyXkFqcGdeQXVyODEyMDIxNDY@._V1_UY268_CR3,0,182,268_AL_.jpg'),
    ('The New Mutants', 'Five young mutants, just discovering their abilities while held in a secret facility against their will, fight to escape their past sins and save themselves.', 'Josh Boone', 2020, 94, 'Now Playing', 'https://m.media-amazon.com/images/M/MV5BZDQ2NTdmNDgtMGIwMS00ODE2LTk5M2EtZGZhYzc4MWRlNTU3XkEyXkFqcGdeQXVyNTc4MjczMTM@._V1_UX182_CR0,0,182,268_AL_.jpg'),
    ('John Wick: Chapter 3 - Parabellum', 'John Wick is on the run after killing a member of the international assassins'' guild, and with a $14 million price tag on his head, he is the target of hit men and women everywhere.', 'Chad Stahelski', 2020, 130, 'Now Playing', 'https://m.media-amazon.com/images/M/MV5BMDg2YzI0ODctYjliMy00NTU0LTkxODYtYTNkNjQwMzVmOTcxXkEyXkFqcGdeQXVyNjg2NjQwMDQ@._V1_UX182_CR0,0,182,268_AL_.jpg'),
    ('The Operative', 'A woman is recruited by the Mossad to work undercover in Tehran.', 'Yuval Adler', 2019, 116, 'Now Playing', 'https://m.media-amazon.com/images/M/MV5BYTgxOTc1ZjctZDE5Ny00OTJhLWE4NjAtYzU2NzE0MjAzNGFkXkEyXkFqcGdeQXVyMTkxNjUyNQ@@._V1_UX182_CR0,0,182,268_AL_.jpg');

--CATEGORY
INSERT INTO category (cat_name)
VALUES 
	('Action'),
	('Horror'),
	('Sci-Fi'),
	('Adventure'),
	('Fantasy'),
	('Animation'),
	('Crime'),
	('Thriller'),
	('Drama'),
	('Mistery'),
	('Comedy');

--MOVIE CATEGORY
INSERT INTO movie_cat (movie_id, category_id)
VALUES
	(1, 1), (1, 4), (1, 5),
	(2, 6), (2, 1), (2, 4),
	(3, 1), (3, 2), (3, 3),
	(4, 1), (4, 7), (4, 8),
	(5, 1), (5, 9), (5, 10);

--DAY SCHEDULE
INSERT INTO day_schedule
(day_name, opening_hours, closing_hours, price)
VALUES
	('Senin', '10:00', '10:00', 20000),
	('Selasa', '10:00', '10:00', 20000),
	('Rabu', '10:00', '10:00', 20000),
	('Kamis', '10:00', '10:00', 20000),
	('Jumat', '10:00', '12:00', 30000),
	('Sabtu', '10:00', '12:00', 30000),
	('Minggu', '10:00', '11:00', 25000);

--STUDIO
INSERT INTO studio
(capacity, layout)
VALUES 
	(50, ARRAY[10, 10, 8, 8, 8, 6]),
	(30, ARRAY[6, 6, 6, 6, 6]),
	(25, ARRAY[5, 5, 5, 5, 5]),
	(25, ARRAY[5, 5, 5, 5, 5]);

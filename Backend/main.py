from flask import Flask, jsonify, request, redirect, url_for, render_template, send_file
from flask_cors import CORS
from sqlalchemy import create_engine
from sqlalchemy.sql import text
from datetime import datetime, date, timedelta
import decimal
import string
import random
import hashlib

app = Flask(__name__)
CORS(app)

conn_str = 'postgresql://postgres:Putranurelva2804?@localhost:5432/watch-it_app'
engine = create_engine(conn_str, echo=False)

########## UTILITY FUNCTIONS ##########
def generate_key():
    letters_and_digits = string.ascii_letters + string.digits
    result_str = ''.join((random.choice(letters_and_digits) for i in range(20)))
    return result_str

def generate_session(_id, _token):
    try:
        with engine.connect() as connection:
            qry = text(f"INSERT INTO public.user_session (user_id, user_token) VALUES ('{_id}', '{_token}')")
            run = connection.execute(qry)
            print("Session Generated")
    except Exception as e:
        return jsonify(str(e))

def user_validation(token):
    res = []
    with engine.connect() as connection:
        qry = text(f"SELECT user_id, first_name, balance FROM public.user_session JOIN public.user USING(user_id) WHERE user_token = '{token}'")
        run = connection.execute(qry)
        for i in run:
            res.append({'user_id' : i[0], 'f_name' : i[1], 'balance' : int(i[2])})
        try:
            return res[0]
        except:
            return jsonify(alert='not found')

                ########## RETRIEVE HTML ##########

########## GENERAL INTERFACE ##########

@app.route('/login', methods=['POST'])
def user_login():
    body = request.form
    _email = body['email']
    _pwd = body['password']
    _pwdencoded = hashlib.sha512(_pwd.encode())

    with engine.connect() as connection:
        validate = text(f"SELECT email_address, password FROM public.user WHERE email_address = '{_email}'")
        runValid = connection.execute(validate)
        for i in runValid:
            user_email = i[0]
            user_pass = i[1]
        try:
            if user_email == 'admin@watchit.admin.com' and user_pass == _pwdencoded.hexdigest():
                return jsonify(alert='admin')
            if user_email == _email and user_pass == _pwdencoded.hexdigest():
                validateUser = text(f"SELECT user_id, first_name, balance FROM public.user WHERE email_address = '{_email}'")
                runValidUser = connection.execute(validateUser)
                for j in runValidUser:
                    _user_id = j[0]
                    _f_name = j[1]
                    _balance = j[2]

                validateSession = text(f"SELECT * FROM public.user_session WHERE user_id='{_user_id}'")
                runValidSession = connection.execute(validateSession)
                for k in runValidSession:
                    user_session = k[0]
                    user_token = k[1]
                    
                try:
                    if user_session == _user_id:
                        return jsonify(alert='ok', token=user_token, user_id=_user_id, f_name=_f_name, balance=_balance)
                except Exception:
                    _token = generate_key()
                    session = generate_session(_user_id, _token)
                        
                    return jsonify(alert='ok', token=_token, user_id=_user_id, f_name=_f_name, balance=_balance)
            else:
                return jsonify(alert="Email and Password does not match")
        except Exception as e:
            print(str(e))
            return jsonify(alert="email not found")

@app.route('/new/user', methods=['POST'])
def create_user():
    body = request.form
    _email = body['email']
    _pwd = str(body['password'])
    _repwd = body['repPassword']
    _first_name = body['f_name']
    _last_name = body['l_name']
    _address = body['address']
    if _pwd != _repwd:
        return jsonify(alert='Re-enter Password does not match the Password')
    else:
        with engine.connect() as connection:
            validate = text(f"SELECT email_address, password FROM public.user WHERE email_address = '{_email}'")
            runValid = connection.execute(validate)

            for i in runValid:
                user_email = i[0]
                user_pass = i[1]
            try:
                if user_email == None or user_email == '':
                    return 'error'
                else :
                    return jsonify(alert='Email address has been used for another account')
            except Exception:
                _pwdencoded = hashlib.sha512(_pwd.encode())

                createAcc = text(f"INSERT INTO public.user (first_name, last_name, email_address, user_address, password)\
                                VALUES ('{_first_name}', '{_last_name}', '{_email}', '{_address}', '{_pwdencoded.hexdigest()}')")
                runCreate = connection.execute(createAcc)

                validateNew = text(f"SELECT user_id, first_name, balance FROM public.user WHERE email_address = '{_email}'")
                runValidNew = connection.execute(validateNew)
                for j in runValidNew:
                    _user_id = j[0]
                    _f_name = j[1]
                    _balance = j[2]

                _token = generate_key()
                session = generate_session(_user_id, _token)

                return jsonify(alert='ok', token=_token, user_id=_user_id, f_name=_f_name, balance=_balance)
                
@app.route('/logout/<token>')
def user_logout(token):
    try:
        with engine.connect() as connection:
            qry = text(f"DELETE FROM public.user_session WHERE user_token = '{token}'")
            run = connection.execute(qry)
        return jsonify(alert='logged out')
    except Exception as e:
        return jsonify(str(e))

########## USER INTERFACE ##########
@app.route('/dashboard/<token>', methods=['GET'])
def userDash(token):

    valid = user_validation(token)
    
    try:
        if valid['user_id'] == None or valid['user_id'] == '':
            return jsonify(alert="Please create an account first")
        else:
            return jsonify(alert='ok', f_name=valid['f_name'], balance=valid['balance'], user_id=valid['user_id'])

    except Exception:
        return jsonify(alert="Please create an account first")

@app.route('/purchase/<schedule_id>/<token>', methods=['GET', 'POST'])
def purchase(schedule_id, token) :
    valid = user_validation(token)
    user_balance = valid['balance']
    user_id = valid['user_id']

    if request.method == 'POST':
        body = [i for i in request.form]
        qty = len(body)
        with engine.connect() as connection:
            movDet = text(f"SELECT price FROM schedule\
                            JOIN movie USING(movie_id)\
                            JOIN day_schedule USING(day_id)\
                            WHERE schedule_id = {schedule_id}")
            runMovDet = connection.execute(movDet)
            for i in runMovDet:
                price = int(i[0])
            totPrice = qty * price
            if user_balance < totPrice:
                return jsonify(alert="insuff bal", user_id=user_id, balance=user_balance, user=valid['f_name'])
            else:
                for i in body:
                    updateSeat = text(f"INSERT INTO seat_taken (schedule_id, seat_id, user_id) VALUES({schedule_id}, '{i}', {user_id})")
                    runUpdateSeat = connection.execute(updateSeat)

                updateTix = text(f'INSERT INTO ticket (schedule_id, user_id, ticket_qty) VALUES({schedule_id}, {user_id}, {qty})')
                runUpdateTix = connection.execute(updateTix)

                updateTrx = text(f"INSERT INTO public.transaction (user_id, purchase) VALUES({user_id}, {totPrice})")
                runUpdateTrx = connection.execute(updateTrx)

                curBal = user_balance - totPrice
                updateBal = text(f"UPDATE public.user SET balance = {curBal} WHERE user_id = {user_id}")
                runUpdateBal = connection.execute(updateBal)

                return jsonify(alert='tix purchased', curBal=curBal)
    else:
        return jsonify(alert="Please create an account first")

@app.route('/profile/<user_id>/edit', methods=['POST'])
def editProfile(user_id):
    user_id = int(user_id)
    body = request.form
    f_name = body['f_name']
    if not f_name:
        f_name = 'NULL'
    l_name = body['l_name']
    if not l_name:
        l_name = 'NULL'
    address = body['address']
    if not address:
        address = 'NULL'
    email_address = body['email_address']
    if not email_address:
        email_address = 'NULL'
    password = body['password']
    if not password:
        password = 'NULL'
    else :
        password = hashlib.sha512(password.encode())
    try:
        with engine.connect() as connection:
            qry = text(f"SELECT first_name, last_name, user_address, email_address, password FROM public.user WHERE user_id={user_id}")
            run = connection.execute(qry)

            if f_name != 'NULL' :
                updateF = text(f"UPDATE public.user SET first_name = '{f_name}' WHERE user_id = {user_id}")
                runUpdateF = connection.execute(updateF)
            if l_name != 'NULL' :
                updateL = text(f"UPDATE public.user SET last_name = '{l_name}' WHERE user_id = {user_id}")
                runUpdateL = connection.execute(updateL)
            if address != 'NULL':
                updateA = text(f"UPDATE public.user SET user_address = '{address}' WHERE user_id = {user_id}")
                runUpdateA = connection.execute(updateA)
            if email_address != 'NULL':
                updateE = text(f"UPDATE public.user SET email_address = '{email_address}' WHERE user_id = {user_id}")
                runUpdateE = connection.execute(updateE)
            if password != 'NULL':
                updateP = text(f"UPDATE public.user SET password = '{password.hexdigest()}' WHERE user_id = {user_id}")
                runUpdateP = connection.execute(updateP)

        return jsonify(msg='profile updated')
            
    except Exception as e:
        return jsonify(str(e))

@app.route('/profile/<user_id>/topup', methods=['POST'])
def userTopUp(user_id):
    try:
        body = request.form
        value = int(body['value'])
        with engine.connect() as connection:
            getBal = text(f"SELECT balance FROM public.user WHERE user_id = {user_id}")
            runGetBal = connection.execute(getBal)

            for i in runGetBal:
                curBal = int(i[0])

            sumBal = curBal + value
            addBal = text(f"UPDATE public.user SET balance = {sumBal} WHERE user_id = {user_id}")
            runAddBal = connection.execute(addBal)

            updateTrx = text(f"INSERT INTO public.transaction (user_id, top_up) VALUES({int(user_id)}, {value})")
            runUpdateTrx = connection.execute(updateTrx)

        return jsonify(msg='topup successful', balance=sumBal)
            
    except Exception as e:
        return jsonify(str(e))

        ###### RETRIEVE JSON ##########

@app.route('/movie/nowplaying', methods=['GET'])
def movie():
    display = []
    try:
        with engine.connect() as connection:
            get_movie = text("SELECT title, description, directed_by, string_agg(cat_name, ', ') as genre, poster_img, trailer FROM movie\
                            JOIN movie_cat USING(movie_id)\
                            JOIN category USING (category_id)\
                            WHERE show_status = 'Now Playing'\
                            GROUP BY 1,2,3,5,6 ORDER BY 1")
            movie_res = connection.execute(get_movie)
            for i in movie_res:
                display.append({"title" : i[0], "description" : i[1], "directed_by" : i[2], "genre" : i[3], "poster" : i[4], "trailer" : i[5]})
            return jsonify(display)

    except Exception as e:
        return jsonify(error=str(e))

@app.route('/movie/<title>', methods=['GET', 'POST'])
def movieDetails(title):
    display = []
    try:
        with engine.connect() as connection:
            get_detail = text(f"SELECT movie_id, title, description, directed_by, release_year, duration, trailer, string_agg(cat_name, ', ') as genre, poster_img FROM movie\
                                    JOIN movie_cat USING(movie_id)\
                                    JOIN category USING (category_id)\
                                    WHERE title = '{title}'\
                                    GROUP BY 1,2,3,4,5,6,7")
            detail_res = connection.execute(get_detail)
            for i in detail_res:
                display.append({"title" : i[1], "desc" : i[2], "directed" : i[3], "release" : i[4], "duration" : i[5], "trailer" : i[6], "genre" : i[7], "poster" : i[8]})
        return jsonify(display)
    except Exception as e:
        return jsonify(error=str(e))

@app.route('/movie/search/<value>', methods=['GET'])
def searchApp(value):
    display = []
    try:
        with engine.connect() as connection:
            if value.isdigit():
                get_detail = text(f"SELECT title, description, directed_by, release_year, duration, string_agg(cat_name, ', '), poster_img\
                            FROM movie JOIN movie_cat USING(movie_id) JOIN category USING (category_id)\
                            WHERE (title LIKE '%{value}%' OR description LIKE '%{value}%' OR directed_by LIKE '%{value}%' OR cat_name LIKE '%{value}%' OR release_year = '{value}')\
                            GROUP BY 1,2,3,4,5,7")
            else:
                get_detail = text(f"SELECT title, description, directed_by, release_year, duration, string_agg(cat_name, ', '), poster_img\
                            FROM movie JOIN movie_cat USING(movie_id) JOIN category USING (category_id)\
                            WHERE (title LIKE '%{value}%' OR description LIKE '%{value}%' OR directed_by LIKE '%{value}%' OR cat_name LIKE '%{value}%')\
                            GROUP BY 1,2,3,4,5,7")

            detail_res = connection.execute(get_detail)
            for i in detail_res:
                display.append({"title" : i[0], "description" : i[1], "directed_by" : i[2], "release_year" : i[3], "duration_mins" : i[4], "genre" : i[5], "poster" : i[6]})
            return jsonify(display)

    except Exception as e:
        return jsonify(str(e))

@app.route('/movie/comingsoon')
def movieUpcoming():
    display = []
    try:
        with engine.connect() as connection:
            qry = text(f"SELECT title, description, directed_by, string_agg(cat_name, ', ') as genre, poster_img\
                            FROM movie\
                            JOIN movie_cat USING(movie_id)\
                            JOIN category USING (category_id)\
                            WHERE show_status = 'Coming Soon'\
                            GROUP BY 1,2,3,5 ORDER BY 1")
            movies = connection.execute(qry)
            for i in movies:
                display.append({"title" : i[0], "description" : i[1], "directed_by" : i[2], "genre" : i[3], "poster" : i[4]})
            return jsonify(display)
    except Exception as e:
        return jsonify(str(e))

@app.route('/movie/mostwatched')
def movieMost_watched():
    display = []
    try:
        with engine.connect() as connection:
            qry = text(f"SELECT m1.title, m1.directed_by, m1.poster_img, \
                            (SELECT string_agg(cat_name, ', ') as genre\
                            FROM movie as m2\
                            JOIN movie_cat USING(movie_id)\
                            JOIN category USING(category_id)\
                            WHERE m1.title = m2.title), SUM(ticket_qty) as total\
                        FROM movie as m1\
                        JOIN schedule as Sched USING(movie_id)\
                        JOIN ticket USING(schedule_id)\
                        GROUP BY 1,2,3\
                        ORDER BY 5 DESC\
                        LIMIT 5")
            movies = connection.execute(qry)
            for i in movies:
                display.append({"title" : i[0], "directed_by" : i[1], "poster" : i[2], "genre" : i[3], "totalTix" : i[4]})
            return jsonify(display)
    except Exception as e:
        return jsonify(str(e))

@app.route('/schedule/<title>', methods=['GET'])
def getSchedule(title):
    display = []
    try:
        with engine.connect() as connection:
            qry = text(f"SELECT play_date, studio_id, start_time, price, schedule_id\
                            FROM schedule\
                            JOIN movie USING(movie_id)\
                            JOIN day_schedule USING(day_id)\
                            WHERE title = '{title}' and play_date = CURRENT_DATE and start_time >= CURRENT_TIME")
            run = connection.execute(qry)
            start_time = []
            schedule_id = []
            for i in run:
                play_date = i[0].strftime("%d %B, %Y")
                studio_id = i[1]
                start_time.append(i[2].strftime("%H:%M"))
                price = int(i[3])
                schedule_id.append(i[4])

            movie = text(f"SELECT title, description, directed_by, string_agg(cat_name, ', ') as genre, poster_img, duration\
                            FROM movie\
                            JOIN movie_cat USING(movie_id)\
                            JOIN category USING (category_id)\
                            WHERE title = '{title}'\
                            GROUP BY 1,2,3,5,6 ORDER BY 1")
            getMovie = connection.execute(movie)
            for j in getMovie:
                title = j[0]
                description = j[1]
                directed_by = j[2]
                genre = j[3]
                img_src = j[4]
                duration_mins = j[5]
            return jsonify(play_date=play_date, studio_id=studio_id, start_time=start_time, price=price, title=title, description=description, directed_by=directed_by, genre=genre, poster=img_src, duration_mins=duration_mins, schedule_id=schedule_id)
            
    except Exception as e:
        return jsonify(alert="schedule not found")

@app.route('/schedule/purchase/<schedule_id>', methods=['GET'])
def purchaseTix(schedule_id):
    display = []
    try:
        with engine.connect() as connection:
            qry = text(f"SELECT play_date, studio_id, start_time, price, movie_id\
                            FROM schedule\
                            JOIN movie USING(movie_id)\
                            JOIN day_schedule USING(day_id)\
                            WHERE schedule_id ={schedule_id}")
            run = connection.execute(qry)
            for i in run:
                play_date = i[0].strftime("%d %B, %Y")
                studio_id = i[1]
                start_time = i[2].strftime("%H:%M")
                price = int(i[3])
                movie_id = i[4]

            movie = text(f"SELECT title, description, directed_by, string_agg(cat_name, ', ') as genre, poster_img, duration\
                            FROM movie\
                            JOIN movie_cat USING(movie_id)\
                            JOIN category USING (category_id)\
                            WHERE movie_id = {movie_id}\
                            GROUP BY 1,2,3,5,6 ORDER BY 1")
            getMovie = connection.execute(movie)
            for j in getMovie:
                title = j[0]
                description = j[1]
                directed_by = j[2]
                genre = j[3]
                poster_img = j[4]
                duration = j[5]
            return jsonify(play_date=play_date, studio_id=studio_id, start_time=start_time, price=price, title=title, description=description, directed_by=directed_by, genre=genre, poster=poster_img, duration_mins=duration, schedule_id=schedule_id)
            
    except Exception as e:
        return jsonify(str(e))
 
@app.route('/seat/<schedule_id>')
def scheduleSeat(schedule_id):
    try:
        with engine.connect() as connection:
            getStud = text(f"SELECT capacity, layout FROM studio\
                            JOIN schedule USING(studio_id)\
                            WHERE schedule_id = {schedule_id}")
            runGetStud = connection.execute(getStud)
            for i in runGetStud:
                capacity = i[0]
                layout = i[1]
            
            seat_taken = []
            taken = text(f"SELECT seat_id FROM seat_taken\
                                WHERE schedule_id = {schedule_id}")
            runTaken = connection.execute(taken)
            for j in runTaken:
                seat_taken.append(j[0])
            
            return jsonify(capacity=capacity, layout=layout, seat_taken=seat_taken)
    except Exception as e:
        return jsonify(str(e))
        
@app.route('/user/<user_id>', methods=['GET'])
def getProfile(user_id):
    try:
        with engine.connect() as connection:
            qry = text(f"SELECT user_id, user_token, first_name, last_name, user_address, email_address, balance FROM public.user_session JOIN public.user USING(user_id) WHERE user_id = {user_id}")
            run = connection.execute(qry)
            for i in run:
                token = i[1]
                f_name = i[2]
                l_name = i[3]
                user_address = i[4]
                email_address = i[5]
                balance = int(i[6])
            return jsonify(user_id=user_id, token=token, f_name=f_name, l_name=l_name, user_address=user_address, email_address=email_address, balance=balance)
    except Exception as e:
        return jsonify(str(e))

@app.route('/user/<user_id>/tix')
def userTix(user_id):
    display = []
    try:
        with engine.connect() as connection:
            qry = text(f"SELECT ticket.user_id, ticket.schedule_id, play_date, title, start_time, ticket_qty, studio_id,\
                            (SELECT ARRAY_AGG(seat_id)\
                                    FROM seat_taken\
                                    WHERE seat_taken.schedule_id = ticket.schedule_id and seat_taken.user_id = ticket.user_id\
                            ) \
                            FROM ticket\
                            JOIN schedule USING(schedule_id)\
                            JOIN movie USING(movie_id)\
                            WHERE ticket.user_id = {user_id}\
                            GROUP BY 1,2,3,4,5,6,7\
                            ORDER BY 2")
            run = connection.execute(qry)
            for i in run:
                if i[2] < date.today() or i[4].strftime("%H:%M:%S") < datetime.now().strftime("%H:%M:%S"):
                    display.append({'play_date': str(i[2]), 'title': i[3], 'start_time': str(i[4]), 'ticket_qty': i[5], 'studio_id': i[6], 'seat_id' : i[7], 'status' : 'done'})
                else:
                    display.append({'play_date': str(i[2]), 'title': i[3], 'start_time': str(i[4]), 'ticket_qty': i[5], 'studio_id': i[6], 'seat_id' : i[7], 'status' : 'active'})
        return jsonify(display)
    except Exception as e:
        return jsonify(str(e))

@app.route('/user/<user_id>/trx')
def userTrx(user_id):
    display = []
    try:
        with engine.connect() as connection:
            qry = text(f"SELECT top_up, purchase, trx_stamp FROM public.transaction WHERE user_id = {int(user_id)}")
            run = connection.execute(qry)
            for i in run:
                display.append({'top_up' : str(i[0]), 'purchase' : str(i[1]), 'time': str(i[2].strftime("%H:%M:%S")), 'date': str(i[2].strftime("%a, %d %b %Y"))})
        return jsonify(display)
    except Exception as e:
        return jsonify(str(e))

@app.route('/admin/tix')
def allTix():
    display = []
    try:
        with engine.connect() as connection:
            qry = text(f"SELECT EXTRACT(year FROM play_date), EXTRACT(month FROM play_date), title, sum(ticket_qty)\
                        FROM ticket JOIN schedule USING(schedule_id) JOIN movie USING(movie_id)\
                        GROUP BY 1,2,3")
            run = connection.execute(qry)
            for i in run:
                display.append({'year': int(i[0]), 'month' : int(i[1]), 'title' : i[2], 'ticket_qty' : i[3]})
            return jsonify(display)
    except Exception as e:
        return jsonify(str(e))

@app.route('/admin/sales')
def movSales():
    display = []
    try:
        with engine.connect() as connection:
            qry = text("SELECT play_date, start_time, movie_id, SUM(ticket_qty)\
                            FROM schedule\
                            JOIN ticket USING(schedule_id)\
                            GROUP BY 1,2,3\
                            ORDER BY 1") 
            run = connection.execute(qry)
            for i in run:
                display.append({'play_date' : str(i[0]), 'start_time' : str(i[1]), 'movie_id' : i[2], 'ticket_sold' : i[3]})
            return jsonify(display)
    except Exception as e:
        return jsonify(str(e))

@app.route('/admin/movie')
def movList():
    display = []
    try:
        with engine.connect() as connection:
            qry = text("SELECT movie_id, title, description, directed_by, release_year, duration, show_status, poster_img, array_agg(cat_name) as genre\
                            FROM movie\
                            JOIN movie_cat USING(movie_id)\
                            JOIN category USING(category_id)\
                            GROUP BY 1,2,3,4,5,6,7,8\
                            ORDER BY 1")
            run = connection.execute(qry)
            for i in run:
                display.append({'movie_id' : i[0], 'title': i[1], 'description': i[2], 'directed_by' : i[3], 'release_year' : i[4], 'duration' : i[5], 'show_status' : i[6], 'poster_img' : i[7], 'genre' : i[8]})
        return jsonify(display)
    except Exception as e:
        return jsonify(str(e))

@app.route('/admin/movie/<movie_id>', methods=['GET', 'POST', 'DELETE'])
def mov_by_id(movie_id):
    display = []
    try:
        with engine.connect() as connection:

            if request.method == 'POST':
                body = request.form
                movie_id = body['movie_id']
                title = body['exTitle'].replace("'","''")
                desc = body['exDesc'].replace("'","''")
                directed_by = body['exDirect']
                release_year = body['exRelease']
                duration = body['exDur']
                show_status = body['exStatus']
                poster_img = body['exPoster']
                genre = body['exGenre'].split(',')
                
                updateMov = text(f"UPDATE movie\
                                    SET title = '{title}', description = '{desc}',\
                                    directed_by = '{directed_by}', release_year = {release_year},\
                                    duration = {duration}, show_status = '{show_status}',\
                                    poster_img = '{poster_img}'\
                                    WHERE movie_id = {movie_id}")
                runUpdateMov = connection.execute(updateMov)

                getPastGen = connection.execute(text(f"SELECT mc_id FROM movie_cat WHERE movie_id = {movie_id}"))
                pastGen = [i[0] for i in getPastGen]
                print(pastGen)
                for i,j in enumerate(genre):
                    print(i,j)
                    updateGenre = text(f"UPDATE movie_cat as mc\
                                        SET category_id = (\
                                        SELECT category_id FROM category as cat\
                                        WHERE cat_name = '{j}')\
                                        WHERE movie_id = {movie_id} AND mc_id = {pastGen[i]}")
                    runUpdateGenre = connection.execute(updateGenre)
                
                return jsonify(alert='movie updated')
            if request.method == 'DELETE':
                qry = text(f"DELETE FROM movie_cat WHERE movie_id = {movie_id};\
                            DELETE FROM movie WHERE movie_id = {movie_id}")
                run = connection.execute(qry)

                return jsonify(alert='movie deleted')

            qry = text(f"SELECT movie_id, title, description, directed_by, release_year, duration, show_status, poster_img, array_agg(cat_name) as genre\
                            FROM movie\
                            JOIN movie_cat USING(movie_id)\
                            JOIN category USING(category_id)\
                            WHERE movie_id = {movie_id}\
                            GROUP BY 1,2,3,4,5,6,7,8\
                            ORDER BY 1")
            run = connection.execute(qry)
            for i in run:
                display.append({'movie_id' : i[0], 'title': i[1], 'description': i[2], 'directed_by' : i[3], 'release_year' : i[4], 'duration' : i[5], 'show_status' : i[6], 'poster_img' : i[7], 'genre' : i[8]})
        return jsonify(display)
    except Exception as e:
        return jsonify(str(e))

@app.route('/admin/movie/add', methods=['POST'])
def mov_add():
    with engine.connect() as connection:
        body = request.form
        title = body['newTitle'].replace("'","''")
        desc = body['newDesc'].replace("'","''")
        directed_by = body['newDirect']
        release_year = body['newRelease']
        duration = body['newDur']
        show_status = body['newStatus']
        poster_img = body['newPoster']
        genre = body['newGenre'].split(',')

        addMov = text(f"INSERT INTO movie\
                        (title, description, directed_by, release_year, duration, show_status, poster_img)\
                        VALUES('{title}', '{desc}', '{directed_by}', {release_year}, {duration}, '{show_status}', '{poster_img}')")
        runAddMov = connection.execute(addMov)

        get_id = text(f"SELECT movie_id FROM movie WHERE title = '{title}'")
        runGet_id = connection.execute(get_id)
        for i in runGet_id:
            movie_id = i[0]
            
        for j in genre:
            addGen = text(f"INSERT INTO movie_cat\
                             (movie_id, category_id)\
                            VALUES({movie_id}, (SELECT category_id FROM category WHERE cat_name = '{j}'))")
            runAddGen = connection.execute(addGen)

        return jsonify(alert='success')

@app.route('/admin/genre')
def genreList():
    display = []
    try:
        with engine.connect() as connection:
            qry = text("SELECT * FROM category ORDER BY 1")
            run = connection.execute(qry)
            for i in run:
                display.append({'category_id': i[0], 'cat_name': i[1]})
        return jsonify(display)
    except Exception as e:
        return jsonify(str(e))

@app.route('/admin/genre/<category_id>', methods=['GET', 'POST', 'DELETE'])
def genre_by_id(category_id):
    display = []
    try:
        with engine.connect() as connection:
            if request.method == 'POST':
                body = request.form
                qry = text(f"UPDATE category SET cat_name = '{body['exGenre']}' WHERE category_id = {category_id}")
                run = connection.execute(qry)
                return jsonify(alert='genre updated')
            elif request.method == 'DELETE':
                qry = text(f"DELETE FROM movie_cat WHERE category_id = {category_id};\
                            DELETE FROM category WHERE category_id = {category_id}")
                run = connection.execute(qry)
                return jsonify(alert='genre deleted')
            else:
                qry = text(f"SELECT * FROM category WHERE category_id = {category_id}")
                run = connection.execute(qry)
                for i in run:
                    display.append({"category_id" : i[0], "cat_name" : i[1]})
                return jsonify(display)
    except Exception as e:
        return jsonify(str(e))

@app.route('/admin/genre/add', methods=['POST'])
def gen_add():
    body = request.form
    cat_name = body['newGenre']
    try:
        with engine.connect() as connection:
            qry = text(f"INSERT INTO category (cat_name) VALUES('{cat_name}')")
            run = connection.execute(qry)

        return jsonify(alert='genre added')
    except Exception as e:
        return jsonify(str(e))

@app.route('/admin/schedule')
def schedList():
    studio = []
    studio_1 = []
    studio_2 = []
    studio_3 = []
    studio_4 = []
    try:
        with engine.connect() as connection:
            stu = text("SELECT * FROM studio")
            runStu = connection.execute(stu)
            for i in runStu:
                studio.append(i[0])

            sched_1 = connection.execute(text(f"SELECT * FROM schedule WHERE studio_id = 1 ORDER BY 1"))
            for j in sched_1:
                studio_1.append({'schedule_id' : j[0], 'day_id' : j[1], 'play_date' : str(j[2]), 'movie_id' : j[3], 'studio_id' : j[4], 'start_time' : str(j[5])})

            sched_2 = connection.execute(text(f"SELECT * FROM schedule WHERE studio_id = 2 ORDER BY 1"))
            for j in sched_2:
                studio_2.append({'schedule_id' : j[0], 'day_id' : j[1], 'play_date' : str(j[2]), 'movie_id' : j[3], 'studio_id' : j[4], 'start_time' : str(j[5])})
   
            sched_3 = connection.execute(text(f"SELECT * FROM schedule WHERE studio_id = 3 ORDER BY 1"))
            for j in sched_3:
                studio_3.append({'schedule_id' : j[0], 'day_id' : j[1], 'play_date' : str(j[2]), 'movie_id' : j[3], 'studio_id' : j[4], 'start_time' : str(j[5])})
  
            sched_4 = connection.execute(text(f"SELECT * FROM schedule WHERE studio_id = 4 ORDER BY 1"))
            for j in sched_4:
                studio_4.append({'schedule_id' : j[0], 'day_id' : j[1], 'play_date' : str(j[2]), 'movie_id' : j[3], 'studio_id' : j[4], 'start_time' : str(j[5])})

            return jsonify(studios=studio, studio_1=studio_1, studio_2=studio_2, studio_3=studio_3, studio_4=studio_4)
    except Exception as e:
        return jsonify(str(e))

@app.route('/admin/schedule/<schedule_id>', methods=['GET', 'POST', 'DELETE'])
def schedule_by_id(schedule_id):
    display = []
    try:
        with engine.connect() as connection:
            if request.method == 'POST':
                body = request.form
                schedule_id = body['schedule_id']
                day_id = body['exDay']
                play_date = body['exDate']
                movie_id = body['exMovie']
                studio_id = body['exStudio']
                start_time = body['exStart1']

                qry = text(f"UPDATE schedule SET day_id = {day_id},\
                            play_date = '{play_date}', movie_id = {movie_id},\
                            studio_id = {studio_id}, start_time = '{start_time}'\
                            WHERE schedule_id = {schedule_id} ")
                run = connection.execute(qry)
                return jsonify(alert='schedule updated')
            if request.method == 'DELETE':
                qry = text(f"DELETE FROM schedule WHERE schedule_id = {schedule_id}")
                run = connection.execute(qry)

                return jsonify(alert='schedule deleted')
            else:
                qry = text(f"SELECT * FROM schedule WHERE schedule_id = {schedule_id}")
                run = connection.execute(qry)
                for i in run:
                    display.append({'schedule_id' : i[0], 'day_id' : i[1], 'play_date' : i[2].strftime('%Y-%m-%d'), 'movie_id' : i[3], 'studio_id' : i[4], 'start_time' : str(i[5])})
                return jsonify(display)
    except Exception as e:
        return jsonify(str(e))

@app.route('/admin/schedule/add', methods=['POST'])
def sched_add():
    try:
        with engine.connect() as connection:
            body = [j for i,j in request.form.items() if j != '']
            times = body[4:]
            days = int(body[3])
            x = 0
            while x < days:
                play_date = datetime.strptime(body[0], "%Y-%m-%d") + timedelta(days=x)
                day_id = play_date.isoweekday()

                for j in times:
                    qry = text(f"INSERT INTO schedule (day_id, play_date, movie_id, studio_id, start_time)\
                                VALUES({day_id}, '{play_date}', {body[1]}, {body[2]}, '{j}')")
                    run = connection.execute(qry)
                x += 1
            return jsonify(alert='schedule added')
    except Exception as e:
        print(e)
        return jsonify(str(e))

@app.route('/admin/user')
def user_list():
    display = []
    try:
        with engine.connect() as connection:
            qry = text("SELECT user_id, first_name, last_name,\
                        email_address, user_address FROM public.user\
                         ORDER BY 1")
            run = connection.execute(qry)
            for i in run:
                display.append({'user_id' : i[0], 'first_name' : i[1], 'last_name' : i[2], 'email_address' : i[3], 'user_address' : i[4]})
            return jsonify(display)
    except Exception as e:
        return jsonify(str(e))

@app.route('/admin/studio')
def studio_list():
    display = []
    try:
        with engine.connect() as connection:
            qry = text("SELECT * FROM studio ORDER BY 1")
            run = connection.execute(qry)
            for i in run:
                display.append({'studio_id' : i[0], 'capacity': i[1], 'layout': i[2]})

            return jsonify(display)
    except Exception as e:
        return jsonify(str(e))

@app.route('/admin/studio/<studio_id>', methods=['GET', 'POST', 'DELETE'])
def studio_by_id(studio_id):
    display = []
    try:
        with engine.connect() as connection:
            if request.method == 'POST':
                body = request.form
                layout = [int(i) for i in body['exLayout'].split(',')]
                capacity = sum(layout)

                qry = text(f"UPDATE studio SET capacity = {capacity}, layout = ARRAY{layout} WHERE studio_id = {studio_id}")
                run = connection.execute(qry)

                return jsonify(alert='studio updated')
            if request.method == 'DELETE':
                qry = text(f"DELETE FROM studio WHERE studio_id = {studio_id}")
                run = connection.execute(qry)

                return jsonify(alert='studio deleted')
            else:
                qry = text(f"SELECT studio_id, layout FROM studio WHERE studio_id = {studio_id}")
                run = connection.execute(qry)

                for i in run:
                    display.append({'studio_id': i[0], 'layout': i[1]})
                return jsonify(display)
    except Exception as e:
        print(e)
        return jsonify(alert='failed')

@app.route('/admin/studio/add', methods=['POST'])
def studio_add():
    body = request.form
    layout = [int(i) for i in body['newLayout'].split(',')]
    capacity = sum(layout)

    try:
        with engine.connect() as connection:
            qry = text(f"INSERT INTO studio (capacity, layout) VALUES ({capacity}, ARRAY{layout})")
            run = connection.execute(qry)
            return jsonify(alert='studio added')

    except Exception as e:
        print(e)
        return jsonify(alert="failed")

@app.route('/admin/day')
def day_schedule():
    display = []
    try:
        with engine.connect() as connection:
            qry = text("SELECT * FROM day_schedule ORDER BY 1")
            run = connection.execute(qry)

            for i in run:
                price = f'{i[4]:,}'.replace(',','.')
                display.append({'day_id': i[0], 'day_name': i[1], 'opening_hours': str(i[2]), 'closing_hours': str(i[3]), 'price': f'{price}'})
            return jsonify(display)

    except Exception as e:
        return jsonify(str(e))

@app.route('/admin/day/<day_id>', methods=['GET', 'POST'])
def day_by_id(day_id):
    display = []
    try:
        with engine.connect() as connection:
            if request.method == 'POST':
                body = request.form
                opening_hours = body['exOpening']
                closing_hours = body['exClosing']
                price = int(body['exPrice'].replace('.',''))

                qry = text(f"UPDATE day_schedule SET opening_hours = '{opening_hours}', closing_hours = '{closing_hours}', price = {price}\
                            WHERE day_id = {day_id}")
                run = connection.execute(qry)

                return jsonify(alert='day updated')

            qry = text(f"SELECT * FROM day_schedule WHERE day_id = {day_id}")
            run = connection.execute(qry)

            for i in run:
                price = f'{i[4]:,}'.replace(',','.')
                display.append({'day_id': i[0], 'day_name': i[1], 'opening_hours': str(i[2]), 'closing_hours': str(i[3]), 'price': f'{price}'})
            return jsonify(display)

    except Exception as e:
        print(e)
        return jsonify(str(e))

if __name__ == '__main__':
    app.run(port=9003)

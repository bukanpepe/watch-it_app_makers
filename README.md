This application is for 2 separate servers, each separated in 2 different folders called 'Frontend' and 'Backend'.
Each folder has their own different environtment.
This application also uses PostgreSQL as its database, and SQLAlchemy to communicate with it.

Installation via terminal:
1. flask
2. psycopg2
3. sqlalchemy
4. flask-sqlalchemy
5. flask-cors
6. requests


The file called 'popDB.sql' is used to initially populate the database with tables schema, and inserted into it some basic data with you can then change or add later through the use of the application.

Once the application runs, you can then log in as the admin and use admin functions to manipulate the datas in the database directly.
For general users, the application then can be used after you have adjusted the database to have a movie schedule with the appropriate and current date.
For seating layout for each studios, you can change them directly in the database inside the 'layout' column, with the lower index value representing the top most row of your studio layout.

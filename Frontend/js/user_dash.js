const defaultUrl = "http://127.0.0.1:5050/dashboard";
const currentUrl = window.location.href;

document.addEventListener('DOMContentLoaded', function () {
    const hiddenTitle = document.getElementById('hidden').innerHTML;
    const user_id = document.getElementById('user_id').innerHTML;
    const f_name = document.getElementById('f_name').innerHTML;
    const cont = document.getElementById('content');
    const slideshowCont = document.getElementsByClassName("slideshow-container")[0];
    const slideDots = document.getElementById('dots');
    const userBalance = document.getElementById('userBalance');
    if (userBalance.innerHTML == 'Rp. 0') {
        var popover = new bootstrap.Popover(userBalance, {
            container: 'body',
            content: 'Your balance appears to be zero, you might want to top up your balance before enjoying our services!',
            title: 'Oops!',
            placement: 'bottom',
            animation: true
        })
        popover.show();
        setTimeout(function () { popover.hide() }, 6000);
    }
    function createBackButton(funcs) {
        let span = document.createElement('span');
        span.className = 'backBtn';

        let backBtn = document.createElement('button');
        backBtn.type = "button";
        backBtn.className = "btn btn-outline-warning";
        backBtn.innerHTML = '&larr; Back';
        span.appendChild(backBtn);
        cont.appendChild(span);

        backBtn.addEventListener('click', function (event) {
            event.preventDefault();
            funcs();
        })
    }

    const modal = document.getElementById('soloModal');
    const modalClose = document.getElementById("modalClose");
    const modalBody = document.getElementById("modalBody");
    const modalTitle = document.getElementById('soloModalLabel');
    const modalFooter = document.getElementById('mdFooter');
    const nowplaying = document.getElementById("npLink");

    modalClose.onclick = function () {
        modal.style.display = "none";
        modalBody.innerHTML = '';
        modalFooter.innerHTML = '';
    }

    const modalCancel = document.createElement('button');
    modalCancel.type = 'button';
    modalCancel.innerHTML = 'Cancel';
    modalCancel.className = 'btn btn-secondary'
    modalCancel.onclick = function () {
        modal.style.display = "none";
        modalBody.innerHTML = '';
        modalFooter.innerHTML = '';
    }

    window.onclick = function (event) {
        if (event.target == modal) {
            modal.style.display = "none";
            modalBody.innerHTML = '';
            modalFooter.innerHTML = '';
        }
    }

    var slideIndex = 1;

    function plusSlides(n) {
        showSlides(slideIndex += n);
    }
    document.getElementsByClassName("prev")[0].addEventListener('click', function (event) {
        event.preventDefault();
        plusSlides(-1);
    });
    document.getElementsByClassName("next")[0].addEventListener('click', function (event) {
        event.preventDefault();
        plusSlides(1);
    });

    function currentSlide(n) {
        showSlides(slideIndex = n);
    }

    async function showSlides(n) {
        var i;
        var slides = document.getElementsByClassName("mySlides");
        var dots = document.getElementsByClassName("dot");
        if (n > slides.length) { slideIndex = 1 }
        if (n < 1) { slideIndex = slides.length }
        for (i = 0; i < slides.length; i++) {
            slides[i].style.display = "none";
        }
        for (i = 0; i < dots.length; i++) {
            dots[i].className = dots[i].className.replace(" active", "");
        }
        slides[slideIndex - 1].style.display = "block";
        dots[slideIndex - 1].className += " active";
    }

    function linkToggleActive(link) {
        let linkList = document.getElementsByClassName("nav-link");
        for (let i = 0; i < linkList.length; i++) {
            linkList[i].classList.remove("active");
        }
        link.classList.add("active");
    }

    function showSnackBar(msg) {
        let snackBar = document.getElementById('snackbar');

        snackBar.innerHTML = msg;
        snackBar.className = 'show';
        setTimeout(function () {
            snackBar.className = '';
            snackBar.innerHTML = '';
        }, 3000);
    }

    nowplaying.addEventListener("click", function (event) {
        event.preventDefault();
        linkToggleActive(nowplaying);
        getMovie();
    })

    if (currentUrl == defaultUrl + '/nowplaying') {
        getMovie();
    }

    function getMovie() {
        history.pushState({}, null, '/dashboard/nowplaying');

        cont.innerHTML = '';
        slideDots.innerHTML = '';
        cont.scrollIntoView();
        const cardDeck = document.createElement("div");
        cardDeck.className = "row row-cols-1 row-cols-md-2 g-5";
        cont.appendChild(cardDeck);

        let i = 0;
        fetch('http://127.0.0.1:9003/movie/nowplaying')
            .then(response => response.json())
            .then(result => {
                while (i < result.length) {

                    let col = document.createElement('div');
                    col.className = 'col';

                    let card = document.createElement('div');
                    card.className = 'card h-100';
                    card.style.maxWidth = '540px';

                    let row = document.createElement('div');
                    row.className = 'row g-0';

                    let cardImg = document.createElement('div');
                    cardImg.className = 'col-md-4';
                    let img = document.createElement('img');
                    let movTitle = result[i].title;
                    img.src = result[i].poster;
                    img.alt = movTitle;
                    img.title = movTitle;
                    img.className = 'card-img';
                    cardImg.appendChild(img);

                    let cardCont = document.createElement('div')
                    cardCont.className = 'col-md-8';

                    let cardBody = document.createElement('div');
                    cardBody.className = 'card-body';

                    let h5 = document.createElement('h4');
                    h5.className = 'card-title';
                    let p1 = document.createElement('p');
                    p1.className = 'card-text';
                    let p2 = document.createElement('p');
                    p2.className = 'card-text';
                    let a = document.createElement('a');

                    a.href = '/movie/' + movTitle;
                    h5.appendChild(a);
                    a.innerHTML = movTitle;
                    a.className = 'card-link'
                    cardBody.appendChild(h5);

                    let movDirect = result[i].directed_by;
                    p1.innerHTML = 'Director : '.bold() + movDirect;
                    cardBody.appendChild(p1);

                    let movGenre = result[i].genre;
                    p2.innerHTML = 'Genre : '.bold() + movGenre;
                    cardBody.appendChild(p2);

                    a.addEventListener('click', function (event) {
                        event.preventDefault();
                        searchMoviebyTitle(movTitle);
                        createBackButton(getMovie);
                    })
                    //slide contents
                    let slideItem = document.createElement('div');
                    slideItem.className = "mySlides";

                    let slideImg = document.createElement('img');
                    slideImg.src = result[i].poster;
                    slideImg.alt = movTitle;
                    slideImg.title = movTitle;
                    slideItem.appendChild(slideImg);

                    let dot = document.createElement('div');
                    dot.className = "dot";
                    doth4 = document.createElement('h5');
                    doth4.innerHTML = movTitle;
                    dot.appendChild(doth4);

                    let trailerBtn = document.createElement('button');
                    let watchIcon = document.createElement('i');
                    watchIcon.className = "fas fa-play";
                    trailerBtn.append(watchIcon, " Watch Trailer");
                    let trailer = result[i].trailer;
                    trailerBtn.addEventListener('click', function (event) {
                        event.preventDefault();
                        window.open(trailer, "_blank");
                    })
                    slideItem.appendChild(trailerBtn);

                    let buyBtn = document.createElement('button');
                    let buyIcon = document.createElement("i");
                    buyIcon.className = "fas fa-ticket-alt";
                    buyBtn.append(buyIcon, " Buy Ticket");
                    buyBtn.addEventListener('click', function (event) {
                        event.preventDefault();
                        event.stopPropagation();
                        scheduleMovie(movTitle);
                    })
                    slideItem.appendChild(buyBtn);

                    slideshowCont.appendChild(slideItem);
                    slideDots.appendChild(dot);
                    cardCont.appendChild(cardBody);
                    row.appendChild(cardImg);
                    row.appendChild(cardCont);
                    card.appendChild(row);
                    col.appendChild(card);
                    cardDeck.appendChild(col);
                    i++;
                }

                showSlides(1);
                let dotList = document.querySelectorAll(".dot")
                for (let j = 0; j < dotList.length; j++) {
                    dotList[j].addEventListener('click', function (event) {
                        event.preventDefault();
                        currentSlide(j + 1);
                    });
                };
            });
    }

    if (currentUrl == defaultUrl + '/' + hiddenTitle.replace(/ /gi, "%20")) {
        searchMoviebyTitle(hiddenTitle);
    }

    function searchMoviebyTitle(title) {
        history.pushState({}, null, '/dashboard/movie/' + title);
        cont.innerHTML = '';

        let url = 'http://127.0.0.1:9003/movie/' + title;
        fetch(url)
            .then(response => response.json())
            .then(result => {
                let card = document.createElement('div');
                card.className = 'card';
                card.style.width = '20rem';
                let cardCont = document.createElement('div')
                cardCont.className = 'card-body';
                let h4 = document.createElement('h4');
                let p1 = document.createElement('p');
                let p2 = document.createElement('p');
                let p3 = document.createElement('p');
                let p4 = document.createElement('p');
                let p5 = document.createElement('p');
                let img = document.createElement('img');

                let movTitle = result[0].title;
                img.src = result[0].poster;
                img.alt = movTitle;
                img.title = movTitle;
                img.className = 'card-img-top';
                card.appendChild(img);

                h4.innerHTML = movTitle;
                h4.className = "card-title";
                cardCont.appendChild(h4);

                let movDesc = result[0].desc;
                p1.innerHTML = movDesc;
                p1.className = "card-text";
                cardCont.appendChild(p1);

                let movDirect = result[0].directed;
                p2.innerHTML = 'Director : '.bold() + movDirect;
                p2.className = "card-text";
                cardCont.appendChild(p2);

                let movGenre = result[0].genre;
                p3.innerHTML = 'Genre : '.bold() + movGenre;
                p3.className = "card-text";
                cardCont.appendChild(p3);

                let movYear = result[0].release;
                p4.innerHTML = 'Release Year : '.bold() + movYear;
                p4.className = "card-text";
                cardCont.appendChild(p4);

                let movDur = result[0].duration;
                p5.innerHTML = 'Duration : '.bold() + movDur + ' mins';
                p4.className = "card-text";
                cardCont.appendChild(p5);

                card.appendChild(cardCont);
                cont.appendChild(card);
            })
    };

    const comingsoon = document.getElementById('csLink');
    comingsoon.addEventListener('click', function (event) {
        event.preventDefault();
        upcomingMovie();
        linkToggleActive(comingsoon);
    })

    if (currentUrl == defaultUrl + '/comingsoon') {
        upcomingMovie();
    }

    function upcomingMovie() {
        history.pushState({}, null, '/dashboard/comingsoon')

        cont.scrollIntoView();
        cont.innerHTML = '';
        let sectBox = document.createElement("div");
        sectBox.className = "sectBox";
        let sectTitle = document.createElement("h3");
        sectTitle.innerHTML = "Upcoming Movies";
        sectBox.appendChild(sectTitle);
        cont.appendChild(sectBox);

        const cardDeck = document.createElement("div");
        cardDeck.className = "row row-cols-1 row-cols-md-2 g-5";
        cont.appendChild(cardDeck);
        let url = 'http://127.0.0.1:9003/movie/comingsoon';
        let i = 0;

        fetch(url)
            .then(response => response.json())
            .then(result => {
                while (i < result.length) {
                    let col = document.createElement('div');
                    col.className = 'col';

                    let card = document.createElement('div');
                    card.className = 'card h-100';
                    card.style.maxWidth = '540px';

                    let row = document.createElement('div');
                    row.className = 'row g-0';

                    let cardImg = document.createElement('div');
                    cardImg.className = 'col-md-4';

                    let h5 = document.createElement('h4');
                    h5.className = "card-title";
                    let p1 = document.createElement('p');
                    p1.className = "card-text";
                    let p2 = document.createElement('p');
                    p2.className = "card-text";

                    let img = document.createElement('img');
                    let a = document.createElement('a');

                    let cardCont = document.createElement('div')
                    cardCont.className = 'col-md-8';

                    let cardBody = document.createElement('div');
                    cardBody.className = 'card-body';

                    let movTitle = result[i].title;
                    img.src = result[i].poster;
                    img.alt = movTitle;
                    img.title = movTitle;
                    img.className = 'card-img';
                    cardImg.appendChild(img);

                    a.href = '/movie/' + movTitle;
                    a.appendChild(h5);
                    h5.innerHTML = movTitle;
                    cardBody.appendChild(a);

                    let movDirect = result[i].directed_by;
                    p1.innerHTML = 'Director : '.bold() + movDirect;
                    cardBody.appendChild(p1);

                    let movGenre = result[i].genre;
                    p2.innerHTML = 'Genre : '.bold() + movGenre;
                    cardBody.appendChild(p2);

                    a.addEventListener('click', function (event) {
                        event.preventDefault();
                        searchMoviebyTitle(movTitle);
                        createBackButton(upcomingMovie);
                    });
                    cardCont.appendChild(cardBody);
                    row.appendChild(cardImg);
                    row.appendChild(cardCont);
                    card.appendChild(row);
                    col.appendChild(card);
                    cardDeck.appendChild(col);
                    i++;
                };
            });
    }

    const mostwatched = document.getElementById('mwLink')
    mostwatched.addEventListener('click', function (event) {
        event.preventDefault();
        mostwatchedMovie();
        linkToggleActive(mostwatched);
    })

    if (currentUrl == defaultUrl + '/mostwatched') {
        mostwatchedMovie();
    }

    function mostwatchedMovie() {
        history.pushState({}, null, '/dashboard/mostwatched')

        cont.scrollIntoView();
        cont.innerHTML = '';
        let sectBox = document.createElement("div");
        sectBox.className = "sectBox";
        let sectTitle = document.createElement("h3");
        sectTitle.innerHTML = "Most Watched Movies";
        sectBox.appendChild(sectTitle);
        cont.appendChild(sectBox);

        const cardDeck = document.createElement("div");
        cardDeck.className = "row row-cols-1 row-cols-md-2 g-5";
        cont.appendChild(cardDeck);
        let url = 'http://127.0.0.1:9003/movie/mostwatched';
        let i = 0;
        fetch(url)
            .then(response => response.json())
            .then(result => {
                while (i < result.length) {
                    let col = document.createElement('div');
                    col.className = 'col';

                    let card = document.createElement('div');
                    card.className = 'card h-100';
                    card.style.maxWidth = '540px';

                    let row = document.createElement('div');
                    row.className = 'row g-0';

                    let cardImg = document.createElement('div');
                    cardImg.className = 'col-md-4';

                    let h5 = document.createElement('h4');
                    h5.className = "card-title";
                    let p1 = document.createElement('p');
                    p1.className = "card-text";
                    let p2 = document.createElement('p');
                    p2.className = "card-text";
                    let p3 = document.createElement('p');
                    p3.className = "card-text";
                    let p4 = document.createElement('p');
                    p4.className = "card-text";
                    let img = document.createElement('img');
                    let a = document.createElement('a');

                    let cardCont = document.createElement('div')
                    cardCont.className = 'col-md-8';

                    let cardBody = document.createElement('div');
                    cardBody.className = 'card-body';

                    let movTitle = result[i].title;
                    img.src = result[i].poster;
                    img.alt = movTitle;
                    img.title = movTitle;
                    img.className = 'card-img';
                    cardImg.appendChild(img);

                    a.href = '/movie/' + movTitle;
                    a.appendChild(h5);
                    h5.innerHTML = movTitle;
                    cardBody.appendChild(a);

                    let movDirect = result[i].directed_by;
                    p2.innerHTML = 'Director : '.bold() + movDirect;
                    cardBody.appendChild(p2);

                    let movGenre = result[i].genre;
                    p3.innerHTML = 'Genre : '.bold() + movGenre;
                    cardBody.appendChild(p3);

                    let sumTix = result[i].totalTix;
                    p4.innerHTML = 'Watched by : ' + sumTix + ' moviegoers';
                    cardBody.appendChild(p4);

                    a.addEventListener('click', function (event) {
                        event.preventDefault();
                        searchMoviebyTitle(movTitle);
                        createBackButton(mostwatchedMovie)
                    })
                    cardCont.appendChild(cardBody);
                    row.appendChild(cardImg);
                    row.appendChild(cardCont);
                    card.appendChild(row);
                    col.appendChild(card);
                    cardDeck.appendChild(col);
                    i++;
                }
            });
    }

    const searchBox = document.getElementById('searchBtn');
    searchBox.addEventListener('click', function (event) {
        event.preventDefault();
        let search = document.getElementById('search').value;
        lookUpMovie(search);
    })

    if (currentUrl == defaultUrl + '/search?q=' + hiddenTitle.replace(/ /gi, "%20")) {
        lookUpMovie(hiddenTitle);
    }

    function lookUpMovie(value) {
        document.getElementById('search').value = '';
        history.pushState({}, null, '/dashboard/search?q=' + value);

        cont.scrollIntoView();
        cont.innerHTML = '';
        let sectBox = document.createElement("div");
        sectBox.className = "sectBox";
        let sectTitle = document.createElement("h3");
        sectTitle.innerHTML = "Search result : " + value;
        sectBox.appendChild(sectTitle);
        cont.appendChild(sectBox);

        const cardDeck = document.createElement("div");
        cardDeck.className = "row row-cols-1 row-cols-md-2 g-5";
        cont.appendChild(cardDeck);
        let url = 'http://127.0.0.1:9003/movie/search/' + value;
        let i = 0;
        fetch(url)
            .then(response => response.json())
            .then(result => {
                while (i < result.length) {
                    let col = document.createElement('div');
                    col.className = 'col';

                    let card = document.createElement('div');
                    card.className = 'card h-100';
                    card.style.maxWidth = '540px';

                    let row = document.createElement('div');
                    row.className = 'row g-0';

                    let cardImg = document.createElement('div');
                    cardImg.className = 'col-md-4';

                    let h5 = document.createElement('h5');
                    h5.className = "card-title";
                    let p1 = document.createElement('p');
                    p1.className = "card-text";
                    let p2 = document.createElement('p');
                    p2.className = "card-text";
                    let p3 = document.createElement('p');
                    p3.className = "card-text";
                    let p4 = document.createElement('p');
                    p4.className = "card-text";
                    let p5 = document.createElement('p');
                    p5.className = "card-text";
                    let img = document.createElement('img');
                    let a = document.createElement('a');

                    let cardCont = document.createElement('div')
                    cardCont.className = 'col-md-8';

                    let cardBody = document.createElement('div');
                    cardBody.className = 'card-body';

                    let movTitle = result[i].title;
                    img.src = result[i].poster;
                    img.alt = movTitle;
                    img.title = movTitle;
                    img.className = 'card-img';
                    cardImg.appendChild(img);

                    a.href = '/movie/' + movTitle;
                    a.appendChild(h5);
                    h5.innerHTML = movTitle;
                    cardBody.appendChild(a);

                    let movDesc = result[i].description;
                    p1.innerHTML = movDesc;
                    cardBody.appendChild(p1);

                    let movDirect = result[i].directed_by;
                    p2.innerHTML = 'Director : '.bold() + movDirect;
                    cardBody.appendChild(p2);

                    let movGenre = result[i].genre;
                    p3.innerHTML = 'Genre : '.bold() + movGenre;
                    cardBody.appendChild(p3);

                    let movYear = result[i].release_year;
                    p4.innerHTML = 'Release Year : '.bold() + movYear;
                    cardBody.appendChild(p4);

                    let movDur = result[i].duration_mins;
                    p5.innerHTML = 'Duration : '.bold() + movDur + ' mins';
                    cardBody.appendChild(p5);

                    a.addEventListener('click', function (event) {
                        event.preventDefault();
                        searchMoviebyTitle(movTitle);
                        createBackButton(getMovie)
                    })
                    cardCont.appendChild(cardBody);
                    row.appendChild(cardImg);
                    row.appendChild(cardCont);
                    card.appendChild(row);
                    col.appendChild(card);
                    cardDeck.appendChild(col);
                    i++;
                }
            });
    }

    const usLink = document.getElementById('usLink');
    const upLink = document.getElementById("upLink");
    upLink.addEventListener('click', function (event) {
        event.preventDefault();
        userProfile();
    })

    if (currentUrl == 'http://127.0.0.1:5050/profile/' + user_id) {
        userProfile();
    }

    function userProfile() {
        linkToggleActive(usLink);
        history.pushState({}, null, '/profile/' + user_id);

        cont.scrollIntoView();
        cont.innerHTML = '';
        let sectBox = document.createElement("div");
        sectBox.className = "sectBox";
        let sectTitle = document.createElement("h3");
        sectTitle.innerHTML = f_name + "'s Profile";
        sectBox.appendChild(sectTitle);
        cont.appendChild(sectBox);
        fetch('http://127.0.0.1:9003/user/' + user_id)
            .then(response => response.json())
            .then(result => {
                let div = document.createElement('div');
                div.className = "user-box";
                let userName = document.createElement('p');
                userName.className = "fs-5 text-dark ms-4";
                let hr1 = document.createElement('hr');
                hr1.className = 'text-secondary';
                let userAdd = document.createElement('p');
                userAdd.className = "fs-5 text-dark ms-4";
                let hr2 = document.createElement('hr');
                hr2.className = 'text-secondary';
                let userEmail = document.createElement('p');
                userEmail.className = "fs-5 text-dark ms-4";
                let hr3 = document.createElement('hr');
                hr3.className = 'text-secondary';
                let userBal = document.createElement('p');
                userBal.className = "fs-5 text-dark ms-4";
                let hr4 = document.createElement('hr');
                hr4.className = 'text-secondary';
                let editBtn = document.createElement('button');
                editBtn.className = 'btn btn-warning btn-outline-dark ms-4 fw-bold'
                editBtn.innerHTML = "Edit Profile";
                editBtn.style.opacity = '0.9';
                if (result.l_name === null) {
                    result.l_name = "";
                } else { result.l_name }

                if (result.user_address === null) {
                    result.user_address = "";
                } else { result.user_address }

                userName.innerHTML = "Name : ".bold() + result.f_name + " " + result.l_name;
                userAdd.innerHTML = "Address : ".bold() + result.user_address;
                userEmail.innerHTML = "E-mail : ".bold() + result.email_address;
                userBal.innerHTML = "Balance : ".bold() + userBalance.innerHTML;
                div.append(userName, hr1, userAdd, hr2, userEmail, hr3, userBal, hr4, editBtn);
                cont.appendChild(div);

                editBtn.addEventListener('click', function (event) {
                    event.preventDefault();
                    event.stopPropagation();
                    editProfile();
                })
            });
    }

    function editProfile() {
        modal.style.display = "block";
        modalBody.innerHTML = '';
        modalFooter.innerHTML = '';
        modalTitle.innerHTML = 'Edit Profile';
        let editForm = document.createElement('form');
        let editF_name = $('<div class="form-floating mb-3">\
                                <input type="text" class="form-control" id="floatingF_name" name="f_name">\
                                <label for="floatingF_name">First Name</label>\
                            </div>');
        let editL_name = $('<div class="form-floating mb-3">\
                            <input type="text" class="form-control" id="floatingL_name" name="l_name">\
                            <label for="floatingL_name">Last Name</label>\
                        </div>');
        let editAddress = $('<div class="form-floating mb-3">\
                                    <input type="text" class="form-control" id="floatingAddress" name="address">\
                                    <label for="floatingAddress">Address</label>\
                                </div>');
        let editEmail = $('<div class="form-floating mb-3">\
                                <input type="email" class="form-control" id="floatingEmail" name="email_address">\
                                <label for="floatingEmail">Email address</label>\
                            </div>');
        let editPassword = $('<div class="form-floating mb-3">\
                                    <input type="password" class="form-control" id="floatingPassword" name="password">\
                                    <label for="floatingEmail">Password</label>\
                                </div>');
        let editBtn = $('<button type="button" class="btn btn-primary">Save changes</button>');

        $(editForm).append(editF_name, editL_name, editAddress, editEmail, editPassword);
        $(modalBody).append(editForm);
        $('#mdFooter').append(editBtn, modalCancel);

        var popover = new bootstrap.Popover(modalTitle, {
            container: 'body',
            content: "You can leave the box empty if you don't need to change the content",
            title: "Don't worry!",
            placement: 'Bottom',
            animation: true
        })
        popover.show();
        setTimeout(function () { popover.hide() }, 4000);

        $(editBtn).click(function (event) {
            event.preventDefault();
            event.stopPropagation();
            fetch(`http://127.0.0.1:5050/profile/${user_id}/edit`, {
                method: 'POST',
                body: new FormData(editForm)
            })
                .then(response => response.json())
                .then(result => {
                    if (result.alert == "profile updated") {
                        showSnackBar("Profile successfully updated");
                        setTimeout(function () {
                            modalClose.click();
                            userProfile();
                        }, 4000);
                    } else if (result.alert == "update failed") {
                        showSnackBar('Profile update failed');
                    }
                })
        });
    }

    const utxLink = document.getElementById('utxLink');
    utxLink.addEventListener('click', function (event) {
        event.stopPropagation();
        event.preventDefault();
        userTix();
    })

    if (currentUrl == 'http://127.0.0.1:5050/profile/' + user_id + '/tix') {
        userTix();
    }

    function userTix() {
        linkToggleActive(usLink);

        cont.scrollIntoView();
        cont.innerHTML = '';
        let sectBox = document.createElement("div");
        sectBox.className = "sectBox";
        let sectTitle = document.createElement("h3");
        sectTitle.innerHTML = "Tickets History";
        sectBox.appendChild(sectTitle);
        cont.appendChild(sectBox);
        fetch(`http://127.0.0.1:5050/profile/${user_id}/tix`)
            .then(response => response.json())
            .then(result => {
                if (result.alert == 'failed') {
                    showSnackBar('Fetch data failed');
                } else {
                    let table = document.createElement('table');
                    table.className = 'table table-light table-striped table-hover';
                    let thead = document.createElement('thead');
                    let headtr = document.createElement('tr');
                    let headno = document.createElement('th');
                    headno.scope = 'col';
                    headno.innerHTML = '#';
                    let headdate = document.createElement('th');
                    headdate.scope = 'col';
                    headdate.innerHTML = 'Date'
                    let headtitle = document.createElement('th');
                    headtitle.scope = 'col';
                    headtitle.innerHTML = 'Movie Title';
                    let headtime = document.createElement('th');
                    headtime.scope = 'col';
                    headtime.innerHTML = 'Schedule Time';
                    let headqty = document.createElement('th');
                    headqty.scope = 'col';
                    headqty.innerHTML = 'Ticket Qty';
                    let headstudio = document.createElement('th');
                    headstudio.scope = 'col';
                    headstudio.innerHTML = 'Studio';
                    let headseat = document.createElement('th');
                    headseat.scope = 'col';
                    headseat.innerHTML = 'Seat';
                    let headstatus = document.createElement('th');
                    headstatus.scope = 'col';
                    headstatus.innerHTML = 'Status';

                    headtr.append(headno, headdate, headtitle, headtime, headqty, headstudio, headseat, headstatus)
                    thead.appendChild(headtr);

                    let tbody = document.createElement('tbody');
                    table.append(thead, tbody);

                    for (let i = 0; i < result.length; i++) {
                        let tr = document.createElement('tr');
                        let no = document.createElement('th');
                        no.scope = 'row';
                        no.innerHTML = i + 1;

                        let date = document.createElement('td');
                        date.innerHTML = result[i].play_date;

                        let title = document.createElement('td');
                        title.innerHTML = result[i].title;

                        let time = document.createElement('td');
                        time.innerHTML = result[i].start_time;

                        let qty = document.createElement('td');
                        qty.innerHTML = result[i].ticket_qty;

                        let studio = document.createElement('td');
                        studio.innerHTML = result[i].studio_id;

                        let seat = document.createElement('td');
                        seat.innerHTML = result[i].seat_id;

                        let statusCell = document.createElement('td');
                        let status = document.createElement('a');
                        if (result[i].status == 'active') {
                            status.className = 'btn btn-success btn-sm';
                            status.innerHTML = result[i].status;
                            statusCell.appendChild(status);
                            status.addEventListener('click', function (event) {
                                event.preventDefault();
                                event.stopPropagation();
                                tixDetail(title.innerHTML, time.innerHTML, studio.innerHTML, seat.innerHTML);
                            })
                        } else {
                            status.className = 'btn btn-secondary btn-sm';
                            status.disabled = true;
                            status.innerHTML = result[i].status;
                            statusCell.appendChild(status);
                        }

                        tr.append(no, date, title, time, qty, studio, seat, statusCell);
                        tbody.appendChild(tr);
                    }
                    cont.appendChild(table);
                }
            })
    }

    function tixDetail(title, time, studio, seat) {

        modalBody.innerHTML = '';
        modalTitle.innerHTML = 'Ticket Detail';
        let div = document.createElement('div');
        div.className = 'mb-3 ms-2';
        let tixTitle = document.createElement('h3');
        tixTitle.innerHTML = title;
        tixTitle.className = 'text-decoration-underline mb-3 fs-3';

        let tixTime = document.createElement('p');
        tixTime.innerHTML = 'Time : '.bold() + time;
        tixTime.className = 'mb-2 fs-4';

        let tixStudio = document.createElement('p');
        tixStudio.innerHTML = 'Studio : '.bold() + studio;
        tixStudio.className = 'mb-2 fs-4'

        let tixSeat = document.createElement('p');
        tixSeat.innerHTML = 'Seat : '.bold();
        tixSeat.className = 'mb-0 fs-4';

        let seatList = document.createElement('span');
        seatList.innerHTML = seat.replace(/,/g, '  ');
        seatList.className = 'mb-3 fs-4';

        let seatBtn = document.createElement('button');
        seatBtn.type = 'button';
        seatBtn.className = 'btn btn-primary';
        seatBtn.style.width = '75px';
        seatBtn.innerHTML = 'OK';

        div.append(tixTitle, tixTime, tixStudio, tixSeat, seatList);
        modalBody.appendChild(div);
        modalFooter.appendChild(seatBtn);
        modal.style.display = 'block';

    }

    const utrLink = document.getElementById('utrLink');
    utrLink.addEventListener('click', function (event) {
        event.stopPropagation();
        event.preventDefault();
        userTrx();
    })

    if (currentUrl == 'http://127.0.0.1:5050/profile/' + user_id + '/trx') {
        userTrx();
    }

    function userTrx() {
        linkToggleActive(usLink);

        cont.scrollIntoView();
        cont.innerHTML = '';
        let sectBox = document.createElement("div");
        sectBox.className = "sectBox";
        let sectTitle = document.createElement("h3");
        sectTitle.innerHTML = "Transactions History";
        sectBox.appendChild(sectTitle);
        cont.appendChild(sectBox);
        fetch(`http://127.0.0.1:5050/profile/${user_id}/trx`)
            .then(response => response.json())
            .then(result => {
                if (result.alert == 'failed') {
                    showSnackBar('Fetch data failed');
                } else {
                    let table = document.createElement('table');
                    table.className = 'table table-light table-striped table-hover';
                    let thead = document.createElement('thead');
                    let headtr = document.createElement('tr');
                    let headno = document.createElement('th');
                    headno.scope = 'col';
                    headno.innerHTML = '#';
                    let headdate = document.createElement('th');
                    headdate.scope = 'col';
                    headdate.innerHTML = 'Date'
                    let headtime = document.createElement('th');
                    headtime.scope = 'col';
                    headtime.innerHTML = 'Time';
                    let headtopup = document.createElement('th');
                    headtopup.scope = 'col';
                    headtopup.innerHTML = 'Top Up';
                    let headtix = document.createElement('th');
                    headtix.scope = 'col';
                    headtix.innerHTML = 'Ticket Purchase';

                    headtr.append(headno, headdate, headtime, headtopup, headtix)
                    thead.appendChild(headtr);

                    let tbody = document.createElement('tbody');
                    table.append(thead, tbody);

                    for (let i = 0; i < result.length; i++) {
                        let tr = document.createElement('tr');
                        let no = document.createElement('th');
                        no.scope = 'row';
                        no.innerHTML = i + 1;

                        let date = document.createElement('td');
                        date.innerHTML = result[i].date;

                        let time = document.createElement('td');
                        time.innerHTML = result[i].time;

                        let topup = document.createElement('td');
                        topup.innerHTML = result[i].top_up.replace(/\B(?=(\d{3})+(?!\d))/g, ".");

                        let tix = document.createElement('td');
                        tix.innerHTML = result[i].purchase.replace(/\B(?=(\d{3})+(?!\d))/g, ".");

                        tr.append(no, date, time, topup, tix);
                        tbody.appendChild(tr);
                    }
                    cont.appendChild(table);
                }
            })
    }

    const utpLink = document.getElementById('utpLink');
    utpLink.addEventListener('click', function (event) {
        event.preventDefault();
        event.stopPropagation();
        userTopUp();
    })

    function userTopUp() {
        modal.style.display = "block";
        modalBody.innerHTML = '';
        modalTitle.innerHTML = 'Top Up Balance';
        let topupForm = document.createElement('form');

        let checks = document.createElement('div');
        checks.className = 'mb-3';

        let topup25kDiv = document.createElement('div');
        topup25kDiv.class = 'form-check gap-3';
        let topup25kInp = document.createElement('input');
        topup25kInp.className = 'form-check-input';
        topup25kInp.type = 'radio';
        topup25kInp.name = 'value';
        topup25kInp.value = '25000';
        topup25kInp.id = 'topup25k';
        let topup25klabel = document.createElement('label');
        topup25klabel.className = 'form-check-label';
        topup25klabel.for = 'topup25k';
        topup25klabel.innerHTML = ' Rp. 25.000'
        topup25kDiv.append(topup25kInp, topup25klabel);

        let topup50kDiv = document.createElement('div');
        topup50kDiv.class = 'form-check';
        let topup50kInp = document.createElement('input');
        topup50kInp.className = 'form-check-input';
        topup50kInp.type = 'radio';
        topup50kInp.name = 'value';
        topup50kInp.value = '50000';
        topup50kInp.id = 'topup50k';
        let topup50klabel = document.createElement('label');
        topup50klabel.className = 'form-check-label';
        topup50klabel.for = 'topup50k';
        topup50klabel.innerHTML = ' Rp. 50.000'
        topup50kDiv.append(topup50kInp, topup50klabel);

        let topup100kDiv = document.createElement('div');
        topup100kDiv.class = 'form-check';
        let topup100kInp = document.createElement('input');
        topup100kInp.className = 'form-check-input';
        topup100kInp.type = 'radio';
        topup100kInp.name = 'value';
        topup100kInp.value = '100000';
        topup100kInp.id = 'topup100k';
        let topup100klabel = document.createElement('label');
        topup100klabel.className = 'form-check-label';
        topup100klabel.for = 'topup100k';
        topup100klabel.innerHTML = ' Rp. 100.000'
        topup100kDiv.append(topup100kInp, topup100klabel);

        checks.append(topup100kDiv, topup50kDiv, topup25kDiv);

        let otherOutDiv = document.createElement('div');
        otherOutDiv.className = 'mb-3';
        let otherLabel = document.createElement('label');
        otherLabel.className = 'form-label';
        otherLabel.for = 'otherAmount';
        otherLabel.innerHTML = 'Other amount :';
        let otherDiv = document.createElement('div');
        otherDiv.className = 'input-group mb-3';
        let otherSpan = document.createElement('span');
        otherSpan.className = 'input-group-text';
        otherSpan.id = 'otherSpan';
        otherSpan.innerHTML = 'Rp.';
        let otherInput = document.createElement('input');
        otherInput.type = 'number';
        otherInput.className = 'form-control';
        otherInput.name = 'value';
        otherInput.id = 'otherAmount';
        otherInput.setAttribute('aria-describedby', 'otherSpan');
        otherDiv.append(otherSpan, otherInput);
        otherOutDiv.append(otherLabel, otherDiv);

        otherInput.addEventListener('focusin', function () {
            topup100kInp.disabled = true;
            topup100kInp.checked = false;
            topup50kInp.disabled = true;
            topup50kInp.checked = false;
            topup25kInp.disabled = true;
            topup25kInp.checked = false;
        })
        otherInput.addEventListener('focusout', function () {
            topup100kInp.disabled = false;
            topup50kInp.disabled = false;
            topup25kInp.disabled = false;
        })

        let topupBtn = $('<button type="button" class="btn btn-primary">Top Up</button>');

        topupForm.append(checks, otherOutDiv);
        modalBody.append(topupForm);
        $('#mdFooter').append(topupBtn, modalCancel);

        $(topupBtn).click(function () {
            fetch(`http://127.0.0.1:5050/profile/${user_id}/topup`, {
                method: 'POST',
                body: new FormData(topupForm)
            })
                .then(response => response.json())
                .then(result => {
                    if (result.alert == 'ok') {
                        showSnackBar('Top up successful!');
                        modalClose.click();
                        setTimeout(function () {
                            window.location.href = '/dashboard'
                        }, 3000)
                    } else {
                        showSnackBar('Top up failed')
                    }
                })
        })
    }

    const uloLink = document.getElementById('uloLink');
    uloLink.addEventListener('click', function (event) {
        event.preventDefault();
        userLogout();
    })

    function userLogout() {
        modal.style.display = "block";
        modalBody.innerHTML = '';
        modalTitle.innerHTML = 'Log Out';
        let text = document.createElement('div');
        text.innerHTML = 'Are you leaving already?';
        modalBody.append(text);

        let conflo = document.createElement('button');
        conflo.className = 'btn btn-primary';
        conflo.innerHTML = 'Confirm';
        conflo.type = 'button';
        modalFooter.append(conflo, modalCancel);

        conflo.addEventListener('click', function (event) {
            event.stopPropagation;
            event.preventDefault();
            window.location.href = '/logout';
        })
    }
    if (currentUrl == defaultUrl) {
        initHome();
    }

    function initHome() {
        linkToggleActive(wiLink);
        getMovie();
        history.pushState({}, null, '/dashboard');


        let sectBox0 = document.createElement("div");
        sectBox0.className = "initBox";
        let sectTitle0 = document.createElement("h3");
        sectTitle0.innerHTML = "Your Profile";
        sectBox0.appendChild(sectTitle0);
        cont.appendChild(sectBox0);

        let profileDiv = document.createElement("div");
        let profileBtn = document.createElement("button");
        profileBtn.type = "button";
        profileBtn.className = "initBtn";
        profileBtn.innerHTML = "Edit your profile, check purchased tickets, and more...";
        profileDiv.appendChild(profileBtn);
        cont.appendChild(profileDiv);
        profileBtn.addEventListener('click', function (event) {
            event.stopPropagation();
            event.preventDefault();
            usLink.click();
            usLink.scrollIntoView();
        })

        let sectBox1 = document.createElement("div");
        sectBox1.className = "initBox";
        let sectTitle1 = document.createElement("h3");
        sectTitle1.innerHTML = "Upcoming Movies";
        sectBox1.appendChild(sectTitle1);
        cont.appendChild(sectBox1);

        let upcomingDiv = document.createElement("div");
        let upcomingBtn = document.createElement("button");
        upcomingBtn.type = "button";
        upcomingBtn.className = "initBtn";
        upcomingBtn.innerHTML = "Browse our upcoming movie list";
        upcomingDiv.appendChild(upcomingBtn);
        cont.appendChild(upcomingDiv);
        upcomingBtn.addEventListener('click', function (event) {
            event.stopPropagation();
            event.preventDefault();
            comingsoon.click();
        })

        let sectBox2 = document.createElement("div");
        sectBox2.className = "initBox";
        let sectTitle2 = document.createElement("h3");
        sectTitle2.innerHTML = "Most Watched Movies";
        sectBox2.appendChild(sectTitle2);
        cont.appendChild(sectBox2);

        let mostwatchedDiv = document.createElement("div");
        let mostwatchedBtn = document.createElement("button");
        mostwatchedBtn.type = "button";
        mostwatchedBtn.className = "initBtn";
        mostwatchedBtn.innerHTML = "Browse our most watched movie list";
        mostwatchedDiv.appendChild(mostwatchedBtn);
        cont.appendChild(mostwatchedDiv);
        mostwatchedBtn.addEventListener('click', function (event) {
            event.stopPropagation();
            event.preventDefault();
            mostwatched.click();
        })
    }


    if (currentUrl == defaultUrl + '/schedule/' + hiddenTitle.replace(/ /gi, "%20")) {
        scheduleMovie(hiddenTitle);
    }

    function scheduleMovie(title) {
        history.pushState({}, null, '/dashboard/schedule/' + title);

        cont.scrollIntoView();
        cont.innerHTML = '';
        let sectBox = document.createElement("div");
        sectBox.className = "sectBox";
        sectBox.style.width = '100%';
        sectBox.style.maxWidth = '1000px';
        sectBox.style.textAlign = 'left';
        let sectTitle = document.createElement("h3");
        sectTitle.innerHTML = "Show schedule : " + title;
        sectTitle.style.marginLeft = '1rem';
        sectBox.appendChild(sectTitle);
        cont.appendChild(sectBox);

        let url = 'http://127.0.0.1:9003/schedule/' + title;
        fetch(url)
            .then(response => response.json())
            .then(result => {
                let card = document.createElement('div');
                card.className = 'card mb-3';
                card.style.maxWidth = '1000px';

                let row = document.createElement('div');
                row.className = 'row g-0';
                let imgCol = document.createElement('div');
                imgCol.className = 'col-md-4';
                let bodyCol = document.createElement('div');
                bodyCol.className = 'col-md-8';

                let cardCont = document.createElement('div')
                cardCont.className = 'card-body';
                let h4 = document.createElement('h4');
                let p1 = document.createElement('p');
                let p2 = document.createElement('p');
                let p3 = document.createElement('p');
                let p4 = document.createElement('p');
                let p5 = document.createElement('p');
                let p6 = document.createElement('p');
                let p7 = document.createElement('p');
                let img = document.createElement('img');
                let br = document.createElement('br');

                if (result.alert == 'schedule not found') {
                    showSnackBar('schedule not available');
                    initHome();
                }

                let movTitle = result.title;
                img.src = result.poster;
                img.alt = movTitle;
                img.title = movTitle;
                img.style.maxWidth = '300px';
                imgCol.appendChild(img);

                h4.innerHTML = movTitle;
                h4.className = 'card-title';
                cardCont.appendChild(h4);

                let movDesc = result.description;
                p1.innerHTML = movDesc;
                p1.className = 'card-text';
                cardCont.appendChild(p1);

                let movDirect = result.directed_by;
                p2.innerHTML = 'Director : '.bold() + movDirect;
                p2.className = 'card-text';
                cardCont.appendChild(p2);

                let movGenre = result.genre;
                p3.innerHTML = 'Genre : '.bold() + movGenre;
                p3.className = 'card-text';
                cardCont.appendChild(p3);

                let movDur = result.duration_mins;
                p4.innerHTML = 'Duration : '.bold() + movDur + ' mins';
                p4.className = 'card-text';
                cardCont.appendChild(p4);
                cardCont.appendChild(document.createElement('hr'));

                let movDate = result.play_date;
                p5.innerHTML = 'DATE : '.bold() + movDate.bold();
                p5.className = 'card-text';
                cardCont.appendChild(p5);

                let movStudio = result.studio_id;
                p6.innerHTML = 'STUDIO ' + movStudio;
                p6.className = 'card-text fw-bold';
                cardCont.appendChild(p6);

                let schedTime = result.start_time;
                let schedID = result.schedule_id;

                let i = 0;
                while (i < schedTime.length) {
                    let a = document.createElement('a');
                    a.className = 'btn btn-outline-dark me-2 mb-2';
                    a.setAttribute('role', 'button');
                    let span = document.createElement('span');
                    let x = schedID[i];

                    a.innerHTML = schedTime[i];
                    a.href = '/purchase/' + x;
                    span.appendChild(a);
                    cardCont.appendChild(span);

                    a.addEventListener('click', function (event) {
                        console.log(x)
                        event.preventDefault();
                        purchaseTix(x);
                        checkSeat(x);
                    })
                    i++;

                }

                let movPrice = result.price;
                p7.innerHTML = 'Rp. ' + movPrice.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                p7.className = 'card-text fs-5 fw-bold';
                cardCont.appendChild(p7);

                bodyCol.appendChild(cardCont);
                row.append(imgCol, bodyCol);
                card.appendChild(row);
                // card.appendChild(cardCont);
                cont.appendChild(card);
            })
    }

    if (currentUrl == defaultUrl + '/purchase/' + hiddenTitle) {
        getMovie();
        purchaseTix(hiddenTitle);
        checkSeat(hiddenTitle);
    }

    function purchaseTix(id) {
        history.pushState({}, null, '/dashboard/purchase/' + id);

        modal.style.display = 'block';
        modalBody.innerHTML = '';
        modalTitle.innerHTML = 'Pick a Seat';

        let url = 'http://127.0.0.1:9003/schedule/purchase/' + id;
        fetch(url)
            .then(response => response.json())
            .then(result => {
                let div = document.createElement('div');
                let h4 = document.createElement('h4');
                let p1 = document.createElement('p');
                let p2 = document.createElement('p');
                let p3 = document.createElement('p');
                let p4 = document.createElement('p');
                let p5 = document.createElement('p');
                let p6 = document.createElement('p');
                let p7 = document.createElement('p');
                let p8 = document.createElement('p');

                div.className = 'mb-3 ms-4';
                let movTitle = result.title;
                h4.innerHTML = movTitle;
                div.appendChild(h4);

                let movDesc = result.description;
                p1.innerHTML = movDesc;
                div.appendChild(p1);

                let movDirect = result.directed_by;
                p2.innerHTML = 'Director : '.bold() + movDirect;
                div.appendChild(p2);

                let movGenre = result.genre;
                p3.innerHTML = 'Genre : '.bold() + movGenre;
                div.appendChild(p3);

                let movDur = result.duration_mins;
                p4.innerHTML = 'Duration : '.bold() + movDur + ' mins';
                div.appendChild(p4);
                div.appendChild(document.createElement('hr'));

                let movDate = result.play_date;
                p5.innerHTML = 'DATE : '.bold() + movDate.bold();
                div.appendChild(p5);

                let movStudio = result.studio_id;
                p6.innerHTML = 'STUDIO ' + movStudio;
                p6.className = 'fw-bold'
                div.appendChild(p6);

                let schedTime = result.start_time;
                p8.innerHTML = schedTime;
                div.appendChild(p8);

                let movPrice = result.price;
                p7.innerHTML = 'Rp. ' + movPrice.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                p7.className = 'fs-5 fw-bold';
                div.appendChild(p7);

                modalBody.appendChild(div);
            })
    }


    function checkSeat(id) {
        setTimeout(function () {
            showSnackBar('Session timeout, page will be refreshed for seat update');
            setTimeout(function () {
                window.location.href = '/dashboard';
            }, 3000);
        }, 60000);

        const outerDiv = document.createElement('div');
        outerDiv.className = 'mb-3';

        const tableDiv = document.createElement('div');
        tableDiv.id = 'seatLayout';
        tableDiv.className = 'seatLayout';

        const table = document.createElement('table');
        table.id = 'seatPlan';

        const screen = document.createElement('div');
        screen.id = 'cine-screen';
        screen.innerHTML = 'SCREEN';
        tableDiv.append(table, screen);
        let url = 'http://127.0.0.1:9003/seat/' + id;

        fetch(url)
            .then(response => response.json())
            .then(result => {
                const layout = result.layout;

                const letter = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']

                for (let i = 0; i < layout.length; i++) {
                    const tr = document.createElement('tr');

                    for (let j = 1; j < layout[i] + 1; j++) {
                        let seat = document.createElement('td');

                        seat.innerHTML = letter[i] + j;
                        seat.id = letter[i] + j;
                        seat.className = 'seat available';
                        tr.appendChild(seat);
                    }
                    table.appendChild(tr);

                }

                let td = document.getElementsByClassName('seat available');
                fetch(url)
                    .then(response => response.json())
                    .then(result => {
                        const taken = result.seat_taken;
                        for (let i = 0; i < td.length; i++) {
                            for (let j = 0; j < taken.length; j++) {
                                if (td[i].id == taken[j]) {
                                    td[i].className = 'seat taken';
                                }
                            }
                        }
                    })


                const seatPick = document.createElement('form');
                seatPick.id = 'seatPick';
                const submit = document.createElement('button');
                submit.type = 'button';
                submit.value = 'Buy Ticket';
                submit.innerHTML = 'Buy Ticket';
                submit.className = 'btn btn-primary mt-2 ms-4';

                const clearPick = document.createElement('button');
                clearPick.type = 'button';
                clearPick.className = 'btn btn-danger mt-2 ms-2';
                clearPick.innerHTML = 'Clear';
                clearPick.id = 'clearPick';

                clearPick.onclick = function () {
                    let select = seatPick.getElementsByTagName('input');
                    let td = document.getElementsByClassName('seat selected');
                    for (let i = 0; i < select.length; i++) {
                        if (select[i].type == 'text') {
                            for (let j = 0; j < td.length; j++) {
                                if (td[j].innerHTML == select[i].value) {
                                    td[j].className = 'seat available';
                                }
                            }
                            seatPick.removeChild(select[i]);
                        }
                    }
                }

                submit.onclick = function () {
                    fetch(`http://127.0.0.1:5050/dashboard/purchase/${id}`, {
                        method: 'POST',
                        body: new FormData(seatPick)
                    })
                        .then(response => response.json())
                        .then(result => {
                            if (result.alert == 'insuff bal') {
                                showSnackBar('Balance is not enough to purchase ticket,\nPlease top up your balance');
                            } else if (result.alert == 'seat taken') {
                                showSnackBar('We are very sorry,\nbut that seat was taken by another user while you were still choosing');
                            } else if (result.alert == 'ok') {
                                showSnackBar('Ticket purchased succesfully!');
                                userBalance.innerHTML = 'Rp. ' + result.curBal;
                                userTix();
                                modalClose.click();
                            }
                        })
                }

                outerDiv.append(tableDiv, seatPick, submit, clearPick);
                modalBody.appendChild(outerDiv);

                for (let x of td) {
                    let pick = document.createElement('input');
                    pick.type = 'text';
                    pick.className = 'mt-2 me-2';
                    if (x.className == 'seat available') {
                        x.addEventListener('click', function () {

                            x.className = 'seat selected';
                            pick.value = x.id;
                            pick.name = x.id;
                            seatPick.appendChild(pick);
                        })
                    }
                }
            })
    }

});
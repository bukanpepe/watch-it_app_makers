document.addEventListener('DOMContentLoaded', function(){
const tixSales = document.getElementById('tixSales');
const movSales = document.getElementById('movSales');
const movList = document.getElementById('movList');
const genreList = document.getElementById('genreList');
const content = document.getElementById('content');
const movEditForm = document.getElementById('movEditForm');
const movAddForm = document.getElementById('movAddForm');
const genEditForm = document.getElementById('genEditForm');
const genAddForm = document.getElementById('genAddForm');
const schedList = document.getElementById('schedList');
const schedEditForm = document.getElementById('schedEditForm');
const schedAddForm = document.getElementById('schedAddForm');
const userList = document.getElementById('userList');
const studioList = document.getElementById('studioList');
const studioEditForm = document.getElementById('studioEditForm');
const studioAddForm = document.getElementById('studioAddForm');
const priceList = document.getElementById('priceList');

//MODALS
const theModal = document.getElementById('myModal');
const modalMsg = document.getElementsByClassName('modal-message')[0];
const modalConfirm = document.getElementById('modal-confirm');
const modalCancel = document.getElementById('modal-cancel');
const modalClose = document.getElementsByClassName('modal-close')[0];
//MODALS

// GLOBAL FUNCTIONS
document.getElementsByClassName('sidenav')[0].addEventListener('mousedown', function(event){
    event.preventDefault();
})

content.addEventListener('mousedown', function(event){
    event.preventDefault();
})

document.getElementsByClassName('modal-content')[0].addEventListener('mousedown', function(event){
    event.preventDefault();
})

function g_closeForm(){
    movAddForm.style.display = 'none';
    movEditForm.style.display = 'none';
    genAddForm.style.display = 'none';
    genEditForm.style.display = 'none';
    schedAddForm.style.display = 'none';
    schedEditForm.style.display = 'none';
    studioAddForm.style.display = 'none';
    studioEditForm.style.display = 'none';
}

function g_toggleActive(obj){
    userList.classList.remove('active');
    tixSales.classList.remove('active');
    movSales.classList.remove('active');
    movList.classList.remove('active');
    addMovBtn.classList.remove('active');
    genreList.classList.remove('active');
    schedList.classList.remove('active');
    addGenBtn.classList.remove('active');
    addSchedBtn.classList.remove('active');
    studioList.classList.remove('active');
    addStudioBtn.classList.remove('active');
    priceList.classList.remove('active');

    obj.classList.toggle('active');
}
modalCancel.addEventListener('click', function(event){
    event.preventDefault();
    modalMsg.innerHTML = '';
    theModal.style.display = 'none';
})

modalClose.addEventListener('click', function(event){
    event.preventDefault();
    modalMsg.innerHTML = '';
    theModal.style.display = 'none';
})

function showSnackBar(msg){
    let snackBar = document.getElementById('snackbar');

    snackBar.innerHTML = msg;
    snackBar.className = 'show';
    setTimeout(function(){
        snackBar.className = '';
        snackBar.innerHTML = '';
    }, 3000);
}
// GLOBAL FUNCTIONS

tixSales.addEventListener('click',function(event){
    event.preventDefault();
    ticketSales();
    g_toggleActive(this);
    history.pushState({}, null, '/admin');
})

function ticketSales() {
    monthList = ['_test', 'January', 'February', 'March', 'April', 'May', 'June', "July", 'August', 'September', 'October', 'November', 'December']
    g_closeForm();
    content.innerHTML = '';
    
    let table = document.createElement('table');
    let tableHead = document.createElement('thead');

    let th1 = document.createElement('th');
    th1.innerHTML = 'year';

    let th2 = document.createElement('th');
    th2.innerHTML = 'month';

    let th3 = document.createElement('th');
    th3.innerHTML = 'title';

    let th4 = document.createElement('th');
    th4.innerHTML = 'ticket_qty';

    tableHead.appendChild(th1);
    tableHead.appendChild(th2);
    tableHead.appendChild(th3);
    tableHead.appendChild(th4);
    table.appendChild(tableHead);

    fetch('http://127.0.0.1:9003/admin/tix')
    .then(response => response.json())
    .then(result => {
        for(let i = 0; i < result.length; i++){
            let tr = document.createElement('tr');
            let td1 = document.createElement('td');
            let td2 = document.createElement('td');
            let td3 = document.createElement('td');
            let td4 = document.createElement('td');

            let year = result[i].year;
            td1.innerHTML = year;

            let month = monthList[result[i].month];
            td2.innerHTML = month;

            let title = result[i].title;
            td3.innerHTML = title;

            let qty = result[i].ticket_qty;
            td4.innerHTML = qty;

            tr.appendChild(td1);
            tr.appendChild(td2);
            tr.appendChild(td3);
            tr.appendChild(td4);
            table.appendChild(tr);
        }
    })
    content.appendChild(table)
}

movSales.addEventListener('click',function(event){
    event.preventDefault();
    movieSales();
    g_toggleActive(this);
    history.pushState({}, null, '/admin');
})

function movieSales() {
    g_closeForm();
    content.innerHTML = '';

    let table = document.createElement('table');
    let tableHead = document.createElement('tr');

    let th1 = document.createElement('th');
    th1.innerHTML = 'play_date';

    let th2 = document.createElement('th');
    th2.innerHTML = 'start_time';

    let th3 = document.createElement('th');
    th3.innerHTML = 'movie_id';

    let th4 = document.createElement('th');
    th4.innerHTML = 'ticket_sold';

    tableHead.appendChild(th1);
    tableHead.appendChild(th2);
    tableHead.appendChild(th3);
    tableHead.appendChild(th4);
    table.appendChild(tableHead);

    fetch('http://127.0.0.1:9003/admin/sales')
    .then(response => response.json())
    .then(result => {
        for(let i = 0; i < result.length; i++){
            let tr = document.createElement('tr');
            let td1 = document.createElement('td');
            let td2 = document.createElement('td');
            let td3 = document.createElement('td');
            let td4 = document.createElement('td');

            let play_date = result[i].play_date;
            td1.innerHTML = play_date;

            let start_time = result[i].start_time;
            td2.innerHTML = start_time;

            let movie_id = result[i].movie_id;
            td3.innerHTML = movie_id;

            let qty = result[i].ticket_sold;
            td4.innerHTML = qty;

            tr.appendChild(td1);
            tr.appendChild(td2);
            tr.appendChild(td3);
            tr.appendChild(td4);
            table.appendChild(tr);
        }
    })
    content.appendChild(table)
}

movList.addEventListener('click',function(event){
    event.preventDefault();
    g_toggleActive(this);
    movieList();
    history.pushState({}, null, '/admin');
})

function movieList() {
    movAddForm.style.display = 'none';
    movEditForm.style.display = 'none';
    genAddForm.style.display = 'none';
    genEditForm.style.display = 'none';
    schedAddForm.style.display = 'none';
    schedEditForm.style.display = 'none';
    content.innerHTML = '';

    let table = document.createElement('table');
    let tableHead = document.createElement('tr');

    let th1 = document.createElement('th');
    th1.innerHTML = 'movie_id';

    let th2 = document.createElement('th');
    th2.innerHTML = 'title';

    let th3 = document.createElement('th');
    th3.innerHTML = 'description';

    let th4 = document.createElement('th');
    th4.innerHTML = 'directed_by';

    let th5 = document.createElement('th');
    th5.innerHTML = 'release_year';

    let th6 = document.createElement('th');
    th6.innerHTML = 'duration';

    let th7 = document.createElement('th');
    th7.innerHTML = 'show_status';

    let th8 = document.createElement('th');
    th8.innerHTML = 'poster_img';

    let th9 = document.createElement('th');
    th9.innerHTML = 'genre';

    tableHead.appendChild(th1);
    tableHead.appendChild(th2);
    tableHead.appendChild(th3);
    tableHead.appendChild(th4);
    tableHead.appendChild(th5);
    tableHead.appendChild(th6);
    tableHead.appendChild(th7);
    tableHead.appendChild(th8);
    tableHead.appendChild(th9);

    table.appendChild(tableHead);

    fetch('http://127.0.0.1:9003/admin/movie')
    .then(response => response.json())
    .then(result => {
        for(let i = 0; i < result.length; i++){
            let tr = document.createElement('tr');
            let td1 = document.createElement('td');
            let td2 = document.createElement('td');
            let td3 = document.createElement('td');
            let td4 = document.createElement('td');
            let td5 = document.createElement('td');
            let td6 = document.createElement('td');
            let td7 = document.createElement('td');
            let td8 = document.createElement('td');
            let td9 = document.createElement('td');
            let td10 = document.createElement('td');
            let td11 = document.createElement('td');

            let movie_id = result[i].movie_id;
            td1.innerHTML = movie_id;

            let title = result[i].title;
            td2.innerHTML = title;

            let description = result[i].description;
            td3.innerHTML = description;

            let directed_by = result[i].directed_by;
            td4.innerHTML = directed_by;

            let release_year = result[i].release_year;
            td5.innerHTML = release_year;

            let duration = result[i].duration;
            td6.innerHTML = duration;

            let show_status = result[i].show_status;
            td7.innerHTML = show_status;

            let poster_img = result[i].poster_img;
            td8.innerHTML = poster_img;

            let genre = result[i].genre;
            td9.innerHTML = genre;

            let td10Btn = document.createElement('button');
            td10Btn.innerHTML = 'Edit';
            td10Btn.addEventListener('click', function(event){
                event.preventDefault();
                editMovie(movie_id);
            })
            td10.appendChild(td10Btn);

            let td11Btn = document.createElement('button');
            td11Btn.innerHTML = 'Delete';
            td11Btn.addEventListener('click', function(event){
                event.preventDefault();
                theModal.style.display = 'block';
                modalMsg.innerHTML = 'Delete ' + title + ' from database?';
                modalConfirm.addEventListener('click', function(event) {
                    event.preventDefault();
                    delMovie(movie_id);
                })
            })
            td11.appendChild(td11Btn);

            tr.appendChild(td1);
            tr.appendChild(td2);
            tr.appendChild(td3);
            tr.appendChild(td4);
            tr.appendChild(td5);
            tr.appendChild(td6);
            tr.appendChild(td7);
            tr.appendChild(td8);
            tr.appendChild(td9);
            tr.appendChild(td10);
            tr.appendChild(td11);

            table.appendChild(tr);
        }
    })
    content.appendChild(table)
}

genreList.addEventListener('click',function(event){
    event.preventDefault();
    g_toggleActive(this);
    movieGenre();
    history.pushState({}, null, '/admin');
})

function movieGenre() {
    movAddForm.style.display = 'none';
    movEditForm.style.display = 'none';
    genAddForm.style.display = 'none';
    genEditForm.style.display = 'none';
    schedAddForm.style.display = 'none';
    schedEditForm.style.display = 'none';
    content.innerHTML = '';

    let table = document.createElement('table');
    let tableHead = document.createElement('tr');

    let th1 = document.createElement('th');
    th1.innerHTML = 'category_id';

    let th2 = document.createElement('th');
    th2.innerHTML = 'category_name';

    tableHead.appendChild(th1);
    tableHead.appendChild(th2);

    table.appendChild(tableHead);

    fetch('http://127.0.0.1:9003/admin/genre')
    .then(response => response.json())
    .then(result => {
        for(let i = 0; i < result.length; i++){
            let tr = document.createElement('tr');
            let td1 = document.createElement('td');
            let td2 = document.createElement('td');
            let td3 = document.createElement('td');
            let td4 = document.createElement('td');
            
            let category_id = result[i].category_id;
            td1.innerHTML = category_id;
             
            let category_name = result[i].cat_name;
            td2.innerHTML = category_name;

            let td3Btn = document.createElement('button');
            td3Btn.innerHTML = 'Edit';
            td3Btn.addEventListener('click', function(event){
                event.preventDefault();
                editGenre(category_id);
            })
            td3.appendChild(td3Btn);

            let td4Btn = document.createElement('button');
            td4Btn.innerHTML = 'Delete';
            td4Btn.addEventListener('click', function(event){
                event.preventDefault();
                theModal.style.display = 'block';
                modalMsg.innerHTML = 'Delete ' + category_name + ' from database?';
                modalConfirm.addEventListener('click', function(event) {
                    event.preventDefault();
                    delGenre(category_id);
                })
            })
            td4.appendChild(td4Btn);

            tr.appendChild(td1);
            tr.appendChild(td2);
            tr.appendChild(td3);
            tr.appendChild(td4);

            table.appendChild(tr);
        }
    })
    content.appendChild(table)
}

function editMovie(id){
    g_closeForm();
    movEditForm.style.display = 'block';

    const movie_id = document.getElementById('movie_id');
    const title = document.getElementById('exTitle');
    title.focus();
    const desc = document.getElementById('exDesc');
    const directed_by = document.getElementById('exDirect');
    const release_year = document.getElementById('exRelease');
    const duration = document.getElementById('exDur');
    const show_status = document.getElementById('exStatus');
    const poster = document.getElementById('exPoster');
    const genre = document.getElementById('exGenre');

    fetch('http://127.0.0.1:9003/admin/movie/' + id)
    .then(response => response.json())
    .then(result => {
        movie_id.value = id;
        title.value = result[0].title;
        desc.value = result[0].description;
        directed_by.value = result[0].directed_by;
        release_year.value = result[0].release_year;
        duration.value = result[0].duration;
        show_status.value = result[0].show_status;
        poster.value = result[0].poster_img;
        genre.value = result[0].genre;
    })
}

const movEditBtn = document.getElementById('movEditBtn');
movEditBtn.addEventListener('click', function(event){
    event.preventDefault();
    theModal.style.display = 'block';
    modalMsg.innerHTML = 'Submit edit to database?';
    modalConfirm.addEventListener('click', function(event) {
        event.preventDefault();
        fetch('/admin/movie/edit', {
            method : 'POST',
            body : new FormData(document.getElementById('movEditForm'))
        })
        .then(response => response.json())
        .then(result => {
            if(result.alert == 'success'){
                showSnackBar('Movie updated successfully');
                movieList();
                theModal.style.display = 'none';
                modalMsg.innerHTML = '';
            } else{
                showSnackBar('Update failed');
                theModal.style.display = 'none';
                modalMsg.innerHTML = '';
            }
        })
    })
})

function delMovie(id) {
    fetch('http://127.0.0.1:9003/admin/movie/' + id, {
        method : 'DELETE'
    })
    .then(response => response.json())
    .then(result =>{
        if(result.alert == 'movie deleted'){
            showSnackBar('Movie deleted successfully');
            movieList();
            theModal.style.display = 'none';
            modalMsg.innerHTML = '';
        } else {
            showSnackBar('Delete failed');
            theModal.style.display = 'none';
            modalMsg.innerHTML = '';
        }
    })
}

const addMovBtn = document.getElementById('addMov');
addMovBtn.addEventListener('click', function(event){
    g_toggleActive(this);
    movEditForm.style.display = 'none';
    history.pushState({}, null, '/admin');
    if(movAddForm.style.display == 'none'){
        movAddForm.style.display = 'block';
        schedAddForm.style.display = 'none';
        genAddForm.style.display = 'none';
        studioAddForm.style.display = 'none';
    } else {
        movAddForm.style.display = 'none';
    }
})

const confAddMov = document.getElementById('movAddBtn');
confAddMov.addEventListener('click', function(event){
    event.preventDefault();
    theModal.style.display = 'block';
    modalMsg.innerHTML = 'Add new movie to the database?';
    modalConfirm.addEventListener('click', function(event){
        event.preventDefault();
        fetch('/admin/movie/add', {
            method : 'POST',
            body : new FormData(document.getElementById('movAddForm'))
        })
        .then(response => response.json())
        .then(result => {
            if(result.alert == 'success'){
                showSnackBar('Movie added successfully');
                movieList();
                theModal.style.display = 'none';
                modalMsg.innerHTML = '';
            } else {
                showSnackBar('Failed to add to database');
                theModal.style.display = 'none';
                modalMsg.innerHTML = '';
            }
        })
    })
})

function editGenre(id){
    g_closeForm();
    genEditForm.style.display = 'block';

    const category_id = document.getElementById('category_id');
    const genre = document.getElementById('exGenre');
    genre.focus();

    fetch('http://127.0.0.1:9003/admin/genre/' + id)
    .then(response => response.json())
    .then(result => {
        category_id.value = id;
        genre.value = result[0].cat_name;
    })
}

const genEditBtn = document.getElementById('genEditBtn');
genEditBtn.addEventListener('click', function(event){
    event.preventDefault();
    theModal.style.display = 'block';
    modalMsg.innerHTML = 'Submit edit to database?';
    modalConfirm.addEventListener('click', function(event) {
        event.preventDefault();
        fetch('/admin/genre/edit', {
            method : 'POST',
            body : new FormData(document.getElementById('genEditForm'))
        })
        .then(response => response.json())
        .then(result => {
            if(result.alert == 'success'){
                showSnackBar('Genre updated successfully');
                movieGenre();
                theModal.style.display = 'none';
                modalMsg.innerHTML = '';
            } else{
                showSnackBar('Update failed');
                theModal.style.display = 'none';
                modalMsg.innerHTML = '';
            }
        })
    })
})

const addGenBtn = document.getElementById('addGen');
addGenBtn.addEventListener('click', function(event){
    g_toggleActive(this);
    genEditForm.style.display = 'none';
    history.pushState({}, null, '/admin');
    if(genAddForm.style.display == 'none'){
        genAddForm.style.display = 'block';
        schedAddForm.style.display = 'none';
        movAddForm.style.display = 'none';
        studioAddForm.style.display = 'none';
    } else {
        genAddForm.style.display = 'none';
    }
})

const confAddGen = document.getElementById('genAddBtn');
confAddGen.addEventListener('click', function(event){
    event.preventDefault();
    theModal.style.display = 'block';
    modalMsg.innerHTML = 'Add new genre to the database?';
    modalConfirm.addEventListener('click', function(event){
        event.preventDefault();
        fetch('/admin/genre/add', {
            method : 'POST',
            body : new FormData(document.getElementById('genAddForm'))
        })
        .then(response => response.json())
        .then(result => {
            if(result.alert == 'success'){
                showSnackBar('Genre added successfully');
                movieGenre();
                theModal.style.display = 'none';
                modalMsg.innerHTML = '';
            } else {
                showSnackBar('Failed to add to database');
                theModal.style.display = 'none';
                modalMsg.innerHTML = '';
            }
        })
    })
})

function delGenre(id) {
    fetch('http://127.0.0.1:9003/admin/genre/' + id, {
        method : 'DELETE'
    })
    .then(response => response.json())
    .then(result =>{
        if(result.alert == 'genre deleted'){
            showSnackBar('Genre deleted successfully');
            movieGenre();
            theModal.style.display = 'none';
            modalMsg.innerHTML = '';
        } else {
            showSnackBar('Delete failed');
            theModal.style.display = 'none';
            modalMsg.innerHTML = '';
        }
    })
}

schedList.addEventListener('click',function(event){
    event.preventDefault();
    g_toggleActive(this);
    scheduleList();
    history.pushState({}, null, '/admin');
})

function scheduleList() {
    g_closeForm();
    content.innerHTML = '';

    fetch('http://127.0.0.1:9003/admin/schedule')
    .then(response => response.json())
    .then(result => {
        for(let i = 0; i < result.studios.length; i++){
            let div = document.createElement('div');
            div.className = 'innerDiv';
            div.innerHTML = 'Studio ' + result.studios[i]

            const table = document.createElement('table');
            let tableHead = document.createElement('tr');

            let th1 = document.createElement('th');
            th1.innerHTML = 'schedule_id';

            let th2 = document.createElement('th');
            th2.innerHTML = 'day_id';

            let th3 = document.createElement('th');
            th3.innerHTML = 'play_date';

            let th4 = document.createElement('th');
            th4.innerHTML = 'movie_id';

            let th5 = document.createElement('th');
            th5.innerHTML = 'studio_id';

            let th6 = document.createElement('th');
            th6.innerHTML = 'start_time';

            tableHead.appendChild(th1);
            tableHead.appendChild(th2);
            tableHead.appendChild(th3);
            tableHead.appendChild(th4);
            tableHead.appendChild(th5);
            tableHead.appendChild(th6);

            table.appendChild(tableHead);

            let x = 'studio_' + result.studios[i];

            for(let j = 0; j < result[x].length; j++ ){
                let tr = document.createElement('tr');
                let td1 = document.createElement('td');
                let td2 = document.createElement('td');
                let td3 = document.createElement('td');
                let td4 = document.createElement('td');
                let td5 = document.createElement('td');
                let td6 = document.createElement('td');
                let td7 = document.createElement('td');
                let td8 = document.createElement('td');

                let schedule_id = result[x][j].schedule_id;
                td1.innerHTML = schedule_id;

                let day_id = result[x][j].day_id;
                td2.innerHTML = day_id;

                let play_date = result[x][j].play_date;
                td3.innerHTML = play_date;

                let movie_id = result[x][j].movie_id;
                td4.innerHTML = movie_id;

                let studio_id = result[x][j].studio_id;
                td5.innerHTML = studio_id;

                let start_time = result[x][j].start_time;
                td6.innerHTML = start_time;

                let td7Btn = document.createElement('button');
                td7Btn.innerHTML = 'Edit';
                td7Btn.addEventListener('click', function(event){
                    event.preventDefault();
                    editSchedule(schedule_id);
                }) 
                td7.appendChild(td7Btn);

                let td8Btn = document.createElement('button');
                td8Btn.innerHTML = 'Delete';
                td8Btn.addEventListener('click', function(event){
                    event.preventDefault();
                    theModal.style.display = 'block';
                    modalMsg.innerHTML = 'Delete schedule_id: ' + schedule_id + ' from database?';
                    modalConfirm.addEventListener('click', function(event) {
                        event.preventDefault();
                        delSchedule(schedule_id);
                    })
                }) 
                td8.appendChild(td8Btn);

                tr.appendChild(td1);
                tr.appendChild(td2);
                tr.appendChild(td3);
                tr.appendChild(td4);
                tr.appendChild(td5);
                tr.appendChild(td6);
                tr.appendChild(td7);
                tr.appendChild(td8);

                table.appendChild(tr);
            }
        content.appendChild(div);
        content.appendChild(table);

        }
    })
}

function editSchedule(id){
    g_closeForm();
    schedEditForm.style.display = 'block';
    
    const schedule_id = document.getElementById('schedule_id');
    const day_id = document.getElementById('exDay');
    day_id.focus();
    const date = document.getElementById('exDate');
    const movie_id = document.getElementById('exMovie');
    const studio_id = document.getElementById('exStudio');
    const start_time1 = document.getElementById('exStart1');

    fetch('http://127.0.0.1:9003/admin/schedule/' + id)
    .then(response => response.json())
    .then(result => {
        schedule_id.value = id;
        day_id.value = result[0].day_id;
        date.value = result[0].play_date;
        movie_id.value = result[0].movie_id;
        studio_id.value = result[0].studio_id;
        start_time1.value = result[0].start_time;
    })
}

const editSchedBtn = document.getElementById('schedEditBtn');
editSchedBtn.addEventListener('click', function(event){
    event.preventDefault();
    theModal.style.display = 'block';
    modalMsg.innerHTML = 'Submit edit to database?';
    modalConfirm.addEventListener('click', function(event) {
        event.preventDefault();
        fetch('/admin/schedule/edit', {
            method : 'POST',
            body : new FormData(document.getElementById('schedEditForm'))
        })
        .then(response => response.json())
        .then(result => {
            if(result.alert == 'success'){
                showSnackBar('Schedule updated successfully');
                scheduleList();
                theModal.style.display = 'none';
                modalMsg.innerHTML = '';
            } else {
                showSnackBar('Update failed');
                theModal.style.display = 'none';
                modalMsg.innerHTML = '';
            }
        })
    })
})

const addSchedBtn = document.getElementById('addSched');
addSchedBtn.addEventListener('click', function(event){
    g_toggleActive(this);
    schedEditForm.style.display = 'none';
    history.pushState({}, null, '/admin');
    if(schedAddForm.style.display == 'none'){
        schedAddForm.style.display = 'block';
        movAddForm.style.display = 'none';
        genAddForm.style.display = 'none';
        studioAddForm.style.display = 'none';
    } else {
        schedAddForm.style.display = 'none';
    }
})

const confAddSched = document.getElementById('schedAddBtn');
confAddSched.addEventListener('click', function(event){
    event.preventDefault();
    theModal.style.display = 'block';
    modalMsg.innerHTML = 'Add new schedule to the database?';
    modalConfirm.addEventListener('click', function(event){
        event.preventDefault();
        fetch('/admin/schedule/add', {
            method : 'POST',
            body : new FormData(document.getElementById('schedAddForm'))
        })
        .then(response => response.json())
        .then(result => {
            if(result.alert == 'success'){
                showSnackBar('Schedule added successfully');
                scheduleList();
                theModal.style.display = 'none';
                modalMsg.innerHTML = '';
            } else {
                showSnackBar('Failed to add to database');
                theModal.style.display = 'none';
                modalMsg.innerHTML = '';
            }
        })
    })
})

function delSchedule(id) {
    fetch('http://127.0.0.1:9003/admin/schedule/' + id, {
        method : 'DELETE'
    })
    .then(response => response.json())
    .then(result =>{
        if(result.alert == 'schedule deleted'){
            showSnackBar('Schedule deleted successfully');
            scheduleList();
            theModal.style.display = 'none';
            modalMsg.innerHTML = '';
        } else {
            showSnackBar('Delete failed');
            theModal.style.display = 'none';
            modalMsg.innerHTML = '';
        }
    })
}

userList.addEventListener('click',function(event){
    event.preventDefault();
    g_toggleActive(this);
    getUserList();
    history.pushState({}, null, '/admin');
})

function getUserList() {
    g_closeForm();
    content.innerHTML = '';

    fetch('http://127.0.0.1:9003/admin/user')
    .then(response => response.json())
    .then(result => {
        const table = document.createElement('table');
        let tableHead = document.createElement('tr');

        let th1 = document.createElement('th');
        th1.innerHTML = 'user_id';

        let th2 = document.createElement('th');
        th2.innerHTML = 'first_name';

        let th3 = document.createElement('th');
        th3.innerHTML = 'last_name';

        let th4 = document.createElement('th');
        th4.innerHTML = 'email_address';

        let th5 = document.createElement('th');
        th5.innerHTML = 'user_address';

        tableHead.appendChild(th1);
        tableHead.appendChild(th2);
        tableHead.appendChild(th3);
        tableHead.appendChild(th4);
        tableHead.appendChild(th5);

        table.appendChild(tableHead);

        for(let i = 0; i < result.length; i++){

            let tr = document.createElement('tr');
            let td1 = document.createElement('td');
            let td2 = document.createElement('td');
            let td3 = document.createElement('td');
            let td4 = document.createElement('td');
            let td5 = document.createElement('td');
            let td6 = document.createElement('td');

            let user_id = result[i].user_id;
            td1.innerHTML = user_id;


            let first_name = result[i].first_name;
            td2.innerHTML = first_name;

            let last_name = result[i].last_name;
            td3.innerHTML = last_name;

            let email_address = result[i].email_address;
            td4.innerHTML = email_address;

            let user_address = result[i].user_address;
            td5.innerHTML = user_address;

            tr.appendChild(td1);
            tr.appendChild(td2);
            tr.appendChild(td3);
            tr.appendChild(td4);
            tr.appendChild(td5);
               
            table.appendChild(tr);
        }
        content.appendChild(table);

    })
}

studioList.addEventListener('click',function(event){
    event.preventDefault();
    g_toggleActive(this);
    getStudioList();
    history.pushState({}, null, '/admin');
})

function getStudioList() {
    g_closeForm();
    content.innerHTML = '';

    fetch('http://127.0.0.1:9003/admin/studio')
    .then(response => response.json())
    .then(result => {
        const table = document.createElement('table');
        let tableHead = document.createElement('tr');

        let th1 = document.createElement('th');
        th1.innerHTML = 'studio_id';

        let th2 = document.createElement('th');
        th2.innerHTML = 'capacity';

        let th3 = document.createElement('th');
        th3.innerHTML = 'layout';

        tableHead.appendChild(th1);
        tableHead.appendChild(th2);
        tableHead.appendChild(th3);

        table.appendChild(tableHead);

        for(let i = 0; i < result.length; i++){

            let tr = document.createElement('tr');
            let td1 = document.createElement('td');
            let td2 = document.createElement('td');
            let td3 = document.createElement('td');
            let td4 = document.createElement('td');
            let td5 = document.createElement('td');

            let studio_id = result[i].studio_id;
            td1.innerHTML = studio_id;

            let capacity = result[i].capacity;
            td2.innerHTML = capacity;

            let layout = result[i].layout;
            td3.innerHTML = layout;

            let td4Btn = document.createElement('button');
            td4Btn.innerHTML = 'Edit';
            td4Btn.addEventListener('click', function(event){
                event.preventDefault();
                editStudio(studio_id);
            }) 
            td4.appendChild(td4Btn);

            let td5Btn = document.createElement('button');
            td5Btn.innerHTML = 'Delete';
            td5Btn.addEventListener('click', function(event){
                event.preventDefault();
                theModal.style.display = 'block';
                modalMsg.innerHTML = 'Delete studio ' + studio_id + ' from database?';
                modalConfirm.addEventListener('click', function(event) {
                    event.preventDefault();
                    delStudio(studio_id);
                })
            }) 
            td5.appendChild(td5Btn);

            tr.appendChild(td1);
            tr.appendChild(td2);
            tr.appendChild(td3);
            tr.appendChild(td4);
            tr.appendChild(td5);

            table.appendChild(tr);
        }
        content.appendChild(table);

    })
}

function editStudio(id){
    g_closeForm();
    studioEditForm.style.display = 'block';

    const studio_id = document.getElementById('studio_id');
    const layout = document.getElementById('exLayout');
    studio_id.focus();

    fetch('http://127.0.0.1:9003/admin/studio/' + id)
    .then(response => response.json())
    .then(result => {
        studio_id.value = id;
        layout.value = result[0].layout;
    })
}

const editStudioBtn = document.getElementById('studioEditBtn');
editStudioBtn.addEventListener('click', function(event){
    event.preventDefault();
    theModal.style.display = 'block';
    modalMsg.innerHTML = 'Submit edit to database?';
    modalConfirm.addEventListener('click', function(event) {
        event.preventDefault();
        fetch('/admin/studio/edit', {
            method : 'POST',
            body : new FormData(document.getElementById('studioEditForm'))
        })
        .then(response => response.json())
        .then(result => {
            if(result.alert == 'success'){
                showSnackBar('Studio updated successfully');
                getStudioList();
                theModal.style.display = 'none';
                modalMsg.innerHTML = '';
            } else {
                showSnackBar('Update failed');
                theModal.style.display = 'none';
                modalMsg.innerHTML = '';
            }
        })
    })
})

const addStudioBtn = document.getElementById('addStudio');
addStudioBtn.addEventListener('click', function(event){
    g_toggleActive(this);
    studioEditForm.style.display = 'none';
    history.pushState({}, null, '/admin');
    if(studioAddForm.style.display == 'none'){
        studioAddForm.style.display = 'block';
        movAddForm.style.display = 'none';
        genAddForm.style.display = 'none';
        schedAddForm.style.display = 'none';
    } else {
        studioAddForm.style.display = 'none';
    }
})

const confAddStudio = document.getElementById('studioAddBtn');
confAddStudio.addEventListener('click', function(event){
    event.preventDefault();
    theModal.style.display = 'block';
    modalMsg.innerHTML = 'Add new studio to the database?';
    modalConfirm.addEventListener('click', function(event){
        event.preventDefault();
        fetch('/admin/studio/add', {
            method : 'POST',
            body : new FormData(document.getElementById('studioAddForm'))
        })
        .then(response => response.json())
        .then(result => {
            if(result.alert == 'success'){
                showSnackBar('Studio added successfully');
                getStudioList();
                theModal.style.display = 'none';
                modalMsg.innerHTML = '';
            } else {
                showSnackBar('Failed to add to database');
                theModal.style.display = 'none';
                modalMsg.innerHTML = '';
            }
        })
    })
})

function delStudio(id) {
    fetch('http://127.0.0.1:9003/admin/studio/' + id, {
        method : 'DELETE'
    })
    .then(response => response.json())
    .then(result =>{
        if(result.alert == 'studio deleted'){
            showSnackBar('Studio deleted successfully');
            getStudioList();
            theModal.style.display = 'none';
            modalMsg.innerHTML = '';
        } else {
            showSnackBar('Delete failed');
            theModal.style.display = 'none';
            modalMsg.innerHTML = '';
        }
    })
}

priceList.addEventListener('click',function(event){
    event.preventDefault();
    g_toggleActive(this);
    dayPrice();
    history.pushState({}, null, '/admin');
})

function dayPrice() {
    g_closeForm();
    content.innerHTML = '';

    let table = document.createElement('table');
    let tableHead = document.createElement('tr');

    let th1 = document.createElement('th');
    th1.innerHTML = 'day_id';

    let th2 = document.createElement('th');
    th2.innerHTML = 'day_name';

    let th3 = document.createElement('th');
    th3.innerHTML = 'opening_hours';

    let th4 = document.createElement('th');
    th4.innerHTML = 'closing_hours';

    let th5 = document.createElement('th');
    th5.innerHTML = 'price';

    tableHead.appendChild(th1);
    tableHead.appendChild(th2);
    tableHead.appendChild(th3);
    tableHead.appendChild(th4);    
    tableHead.appendChild(th5);

    table.appendChild(tableHead);

    fetch('http://127.0.0.1:9003/admin/day')
    .then(response => response.json())
    .then(result => {
        for(let i = 0; i < result.length; i++){
            let tr = document.createElement('tr');
            let td1 = document.createElement('td');
            let td2 = document.createElement('td');
            let td3 = document.createElement('td');
            let td4 = document.createElement('td');
            let td5 = document.createElement('td');
            let td6 = document.createElement('td');

            let day_id = result[i].day_id;
            td1.innerHTML = day_id;
             
            let day_name = result[i].day_name;
            td2.innerHTML = day_name;

            let opening_hours = result[i].opening_hours;
            td3.innerHTML = opening_hours;
             
            let closing_hours = result[i].closing_hours;
            td4.innerHTML = closing_hours;
       
            let price = result[i].price;
            td5.innerHTML = price;

            let td6Btn = document.createElement('button');
            td6Btn.innerHTML = 'Edit';
            td6Btn.addEventListener('click', function(event){
                event.preventDefault();
                editDay(day_id);
            })
            td6.appendChild(td6Btn);

            tr.appendChild(td1);
            tr.appendChild(td2);
            tr.appendChild(td3);
            tr.appendChild(td4);
            tr.appendChild(td5);
            tr.appendChild(td6);

            table.appendChild(tr);
        }
    })
    content.appendChild(table)
}

function editDay(id){
    g_closeForm();
    dayEditForm.style.display = 'block';

    const day_id = document.getElementById('day_id');
    const opening_hours = document.getElementById('exOpening');
    day_id.focus();
    const closing_hours = document.getElementById('exClosing');
    const price = document.getElementById('exPrice');

    fetch('http://127.0.0.1:9003/admin/day/' + id)
    .then(response => response.json())
    .then(result => {
        day_id.value = id;
        opening_hours.value = result[0].opening_hours;
        closing_hours.value = result[0].closing_hours;
        price.value = result[0].price;

    })
}

const editDayBtn = document.getElementById('dayEditBtn');
editDayBtn.addEventListener('click', function(event){
    event.preventDefault();
    theModal.style.display = 'block';
    modalMsg.innerHTML = 'Submit edit to database?';
    modalConfirm.addEventListener('click', function(event) {
        event.preventDefault();
        fetch('/admin/day/edit', {
            method : 'POST',
            body : new FormData(document.getElementById('dayEditForm'))
        })
        .then(response => response.json())
        .then(result => {
            if(result.alert == 'success'){
                showSnackBar('Day updated successfully');
                dayPrice();
                theModal.style.display = 'none';
                modalMsg.innerHTML = '';
            } else {
                showSnackBar('Update failed');
                theModal.style.display = 'none';
                modalMsg.innerHTML = '';
            }
        })
    })
})

const numDay = document.getElementById('numDay');
function addDay(num) {
    numDay.value = num;
    confAddSched.click();
}

const add1Day = document.getElementById('add1Day');
add1Day.addEventListener('click', function(event){
    event.preventDefault();
    addDay(1);
})

const add4Day = document.getElementById('add4Day');
add4Day.addEventListener('click', function(event){
    event.preventDefault();
    addDay(4);
})

const add7Day = document.getElementById('add7Day');
add7Day.addEventListener('click', function(event){
    event.preventDefault();
    addDay(7);
})

var getAlert = document.getElementById('alert').innerHTML;
if (getAlert !== "") {
    showAlert(getAlert)
}

function showAlert(msg) {
    alert(msg)
}

})
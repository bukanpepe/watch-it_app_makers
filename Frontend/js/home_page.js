const defaultUrl = "http://127.0.0.1:5050";
const currentUrl = window.location.href;
document.addEventListener('DOMContentLoaded', function () {
    const hiddenTitle = document.getElementById('hidden').innerHTML;
    const cont = document.getElementById('content');
    const slideshowCont = document.getElementsByClassName("slideshow-container")[0];
    const slideDots = document.getElementById('dots');

    function createBackButton(funcs) {
        let span = document.createElement('span');
        span.className = 'backBtn';

        let backBtn = document.createElement('button');
        backBtn.type = "button";
        backBtn.className = "btn btn-outline-warning";
        backBtn.innerHTML = '&larr; Back';
        span.appendChild(backBtn);
        cont.appendChild(span);

        backBtn.addEventListener('click', function (event) {
            event.preventDefault();
            funcs();
        })
    }

    const nowplaying = document.getElementById("npLink");
    nowplaying.addEventListener("click", function (event) {
        event.preventDefault();
        linkToggleActive(nowplaying);
        getMovie();
    })

    if (currentUrl == defaultUrl + '/nowplaying') {
        getMovie();
    }

    function getMovie() {
        history.pushState({}, null, '/nowplaying');

        cont.innerHTML = '';
        slideDots.innerHTML = '';
        cont.scrollIntoView();
        const cardDeck = document.createElement("div");
        cardDeck.className = "row row-cols-1 row-cols-md-2 g-5";
        cont.appendChild(cardDeck);

        let i = 0;
        fetch('http://127.0.0.1:9003/movie/nowplaying')
            .then(response => response.json())
            .then(result => {
                while (i < result.length) {

                    let col = document.createElement('div');
                    col.className = 'col';

                    let card = document.createElement('div');
                    card.className = 'card h-100';
                    card.style.maxWidth = '540px';

                    let row = document.createElement('div');
                    row.className = 'row g-0';

                    let cardImg = document.createElement('div');
                    cardImg.className = 'col-md-4';
                    let img = document.createElement('img');
                    let movTitle = result[i].title;
                    img.src = result[i].poster;
                    img.alt = movTitle;
                    img.title = movTitle;
                    img.className = 'card-img';
                    cardImg.appendChild(img);

                    let cardCont = document.createElement('div')
                    cardCont.className = 'col-md-8';

                    let cardBody = document.createElement('div');
                    cardBody.className = 'card-body';

                    let h5 = document.createElement('h4');
                    h5.className = 'card-title';
                    let p1 = document.createElement('p');
                    p1.className = 'card-text';
                    let p2 = document.createElement('p');
                    p2.className = 'card-text';
                    let a = document.createElement('a');

                    a.href = '/movie/' + movTitle;
                    h5.appendChild(a);
                    a.innerHTML = movTitle;
                    a.className = 'card-link'
                    cardBody.appendChild(h5);

                    let movDirect = result[i].directed_by;
                    p1.innerHTML = 'Director : '.bold() + movDirect;
                    cardBody.appendChild(p1);

                    let movGenre = result[i].genre;
                    p2.innerHTML = 'Genre : '.bold() + movGenre;
                    cardBody.appendChild(p2);

                    a.addEventListener('click', function (event) {
                        event.preventDefault();
                        event.stopPropagation();
                        searchMoviebyTitle(movTitle);
                        createBackButton(getMovie);
                    })
                    //slide contents
                    let slideItem = document.createElement('div');
                    slideItem.className = "mySlides";

                    let slideImg = document.createElement('img');
                    slideImg.src = result[i].poster;
                    slideImg.alt = movTitle;
                    slideImg.title = movTitle;
                    slideItem.appendChild(slideImg);

                    let dot = document.createElement('div');
                    dot.className = "dot";
                    doth4 = document.createElement('h5');
                    doth4.innerHTML = movTitle;
                    dot.appendChild(doth4);

                    let trailerBtn = document.createElement('button');
                    let watchIcon = document.createElement('i');
                    watchIcon.className = "fas fa-play";
                    trailerBtn.append(watchIcon, " Watch Trailer");
                    let trailer = result[i].trailer;
                    trailerBtn.addEventListener('click', function (event) {
                        event.preventDefault();
                        window.open(trailer, "_blank");
                    })
                    slideItem.appendChild(trailerBtn);

                    let buyBtn = document.createElement('button');
                    let buyIcon = document.createElement("i");
                    buyIcon.className = "fas fa-ticket-alt";
                    buyBtn.append(buyIcon, " Buy Ticket");
                    buyBtn.addEventListener('click', function (event) {
                        event.preventDefault();
                        event.stopPropagation();
                        userLogin();
                    })
                    slideItem.appendChild(buyBtn);

                    slideshowCont.appendChild(slideItem);
                    slideDots.appendChild(dot);
                    cardCont.appendChild(cardBody);
                    row.appendChild(cardImg);
                    row.appendChild(cardCont);
                    card.appendChild(row);
                    col.appendChild(card);
                    cardDeck.appendChild(col);
                    i++;
                }
                showSlides(1);
                let dotList = document.querySelectorAll(".dot")
                for (let j = 0; j < dotList.length; j++) {
                    dotList[j].addEventListener('click', function (event) {
                        event.preventDefault();
                        currentSlide(j + 1);
                    });
                };
            });
    };

    if (currentUrl == defaultUrl + '/movie/' + hiddenTitle.replace(/ /gi, "%20")) {
        searchMoviebyTitle(hiddenTitle);
    }

    function searchMoviebyTitle(title) {
        history.pushState({}, null, '/movie/' + title);
        cont.innerHTML = '';

        let url = 'http://127.0.0.1:9003/movie/' + title;
        fetch(url)
            .then(response => response.json())
            .then(result => {
                let card = document.createElement('div');
                card.className = 'card';
                card.style.width = '20rem';
                let cardCont = document.createElement('div')
                cardCont.className = 'card-body';
                let h4 = document.createElement('h4');
                let p1 = document.createElement('p');
                let p2 = document.createElement('p');
                let p3 = document.createElement('p');
                let p4 = document.createElement('p');
                let p5 = document.createElement('p');
                let img = document.createElement('img');

                let movTitle = result[0].title;
                img.src = result[0].poster;
                img.alt = movTitle;
                img.title = movTitle;
                img.className = 'card-img-top';
                card.appendChild(img);

                h4.innerHTML = movTitle;
                h4.className = "card-title";
                cardCont.appendChild(h4);

                let movDesc = result[0].desc;
                p1.innerHTML = movDesc;
                p1.className = "card-text";
                cardCont.appendChild(p1);

                let movDirect = result[0].directed;
                p2.innerHTML = 'Director : '.bold() + movDirect;
                p2.className = "card-text";
                cardCont.appendChild(p2);

                let movGenre = result[0].genre;
                p3.innerHTML = 'Genre : '.bold() + movGenre;
                p3.className = "card-text";
                cardCont.appendChild(p3);

                let movYear = result[0].release;
                p4.innerHTML = 'Release Year : '.bold() + movYear;
                p4.className = "card-text";
                cardCont.appendChild(p4);

                let movDur = result[0].duration;
                p5.innerHTML = 'Duration : '.bold() + movDur + ' mins';
                p4.className = "card-text";
                cardCont.appendChild(p5);

                card.appendChild(cardCont);
                cont.appendChild(card);
            })
    };

    const comingsoon = document.getElementById('csLink');
    comingsoon.addEventListener('click', function (event) {
        event.preventDefault();
        upcomingMovie();
        linkToggleActive(comingsoon);
    })

    if (currentUrl == defaultUrl + '/movie/comingsoon') {
        upcomingMovie();
    }

    function upcomingMovie() {
        history.pushState({}, null, '/movie/comingsoon')

        cont.scrollIntoView();
        cont.innerHTML = '';
        let sectBox = document.createElement("div");
        sectBox.className = "sectBox";
        let sectTitle = document.createElement("h3");
        sectTitle.innerHTML = "Upcoming Movies";
        sectBox.appendChild(sectTitle);
        cont.appendChild(sectBox);

        const cardDeck = document.createElement("div");
        cardDeck.className = "row row-cols-1 row-cols-md-2 g-5";
        cont.appendChild(cardDeck);
        let url = 'http://127.0.0.1:9003/movie/comingsoon';
        let i = 0;

        fetch(url)
            .then(response => response.json())
            .then(result => {
                while (i < result.length) {
                    let col = document.createElement('div');
                    col.className = 'col';

                    let card = document.createElement('div');
                    card.className = 'card h-100';
                    card.style.maxWidth = '540px';

                    let row = document.createElement('div');
                    row.className = 'row g-0';

                    let cardImg = document.createElement('div');
                    cardImg.className = 'col-md-4';

                    let h5 = document.createElement('h4');
                    h5.className = "card-title";
                    let p1 = document.createElement('p');
                    p1.className = "card-text";
                    let p2 = document.createElement('p');
                    p2.className = "card-text";

                    let img = document.createElement('img');
                    let a = document.createElement('a');

                    let cardCont = document.createElement('div')
                    cardCont.className = 'col-md-8';

                    let cardBody = document.createElement('div');
                    cardBody.className = 'card-body';

                    let movTitle = result[i].title;
                    img.src = result[i].poster;
                    img.alt = movTitle;
                    img.title = movTitle;
                    img.className = 'card-img';
                    cardImg.appendChild(img);

                    a.href = '/movie/' + movTitle;
                    a.appendChild(h5);
                    h5.innerHTML = movTitle;
                    cardBody.appendChild(a);

                    let movDirect = result[i].directed_by;
                    p1.innerHTML = 'Director : '.bold() + movDirect;
                    cardBody.appendChild(p1);

                    let movGenre = result[i].genre;
                    p2.innerHTML = 'Genre : '.bold() + movGenre;
                    cardBody.appendChild(p2);

                    a.addEventListener('click', function (event) {
                        event.preventDefault();
                        searchMoviebyTitle(movTitle);
                        createBackButton(upcomingMovie);
                    });
                    cardCont.appendChild(cardBody);
                    row.appendChild(cardImg);
                    row.appendChild(cardCont);
                    card.appendChild(row);
                    col.appendChild(card);
                    cardDeck.appendChild(col);
                    i++;
                };
            });
    }

    const mostwatched = document.getElementById('mwLink')
    mostwatched.addEventListener('click', function (event) {
        event.preventDefault();
        mostwatchedMovie();
        linkToggleActive(mostwatched);
    })

    if (currentUrl == defaultUrl + '/movie/mostwatched') {
        mostwatchedMovie();
    }

    function mostwatchedMovie() {
        history.pushState({}, null, '/movie/mostwatched')

        cont.scrollIntoView();
        cont.innerHTML = '';
        let sectBox = document.createElement("div");
        sectBox.className = "sectBox";
        let sectTitle = document.createElement("h3");
        sectTitle.innerHTML = "Most Watched Movies";
        sectBox.appendChild(sectTitle);
        cont.appendChild(sectBox);

        const cardDeck = document.createElement("div");
        cardDeck.className = "row row-cols-1 row-cols-md-2 g-5";
        cont.appendChild(cardDeck);
        let url = 'http://127.0.0.1:9003/movie/mostwatched';
        let i = 0;
        fetch(url)
            .then(response => response.json())
            .then(result => {
                while (i < result.length) {
                    let col = document.createElement('div');
                    col.className = 'col';

                    let card = document.createElement('div');
                    card.className = 'card h-100';
                    card.style.maxWidth = '540px';

                    let row = document.createElement('div');
                    row.className = 'row g-0';

                    let cardImg = document.createElement('div');
                    cardImg.className = 'col-md-4';

                    let h5 = document.createElement('h4');
                    h5.className = "card-title";
                    let p1 = document.createElement('p');
                    p1.className = "card-text";
                    let p2 = document.createElement('p');
                    p2.className = "card-text";
                    let p3 = document.createElement('p');
                    p3.className = "card-text";
                    let p4 = document.createElement('p');
                    p4.className = "card-text";
                    let img = document.createElement('img');
                    let a = document.createElement('a');

                    let cardCont = document.createElement('div')
                    cardCont.className = 'col-md-8';

                    let cardBody = document.createElement('div');
                    cardBody.className = 'card-body';

                    let movTitle = result[i].title;
                    img.src = result[i].poster;
                    img.alt = movTitle;
                    img.title = movTitle;
                    img.className = 'card-img';
                    cardImg.appendChild(img);

                    a.href = '/movie/' + movTitle;
                    a.appendChild(h5);
                    h5.innerHTML = movTitle;
                    cardBody.appendChild(a);

                    let movDirect = result[i].directed_by;
                    p2.innerHTML = 'Director : '.bold() + movDirect;
                    cardBody.appendChild(p2);

                    let movGenre = result[i].genre;
                    p3.innerHTML = 'Genre : '.bold() + movGenre;
                    cardBody.appendChild(p3);

                    let sumTix = result[i].totalTix;
                    p4.innerHTML = 'Watched by : ' + sumTix + ' moviegoers';
                    cardBody.appendChild(p4);

                    a.addEventListener('click', function (event) {
                        event.preventDefault();
                        searchMoviebyTitle(movTitle);
                        createBackButton(mostwatchedMovie)
                    })
                    cardCont.appendChild(cardBody);
                    row.appendChild(cardImg);
                    row.appendChild(cardCont);
                    card.appendChild(row);
                    col.appendChild(card);
                    cardDeck.appendChild(col);
                    i++;
                }
            });
    }

    const searchBox = document.getElementById('searchBtn');
    searchBox.addEventListener('click', function (event) {
        event.preventDefault();
        let search = document.getElementById('search').value;
        lookUpMovie(search);
    })

    if (currentUrl == defaultUrl + '/movie/search?q=' + hiddenTitle.replace(/ /gi, "%20")) {
        lookUpMovie(hiddenTitle);
    }

    function lookUpMovie(value) {
        document.getElementById('search').value = '';
        history.pushState({}, null, '/movie/search?q=' + value);

        cont.scrollIntoView();
        cont.innerHTML = '';
        let sectBox = document.createElement("div");
        sectBox.className = "sectBox";
        let sectTitle = document.createElement("h3");
        sectTitle.innerHTML = "Search result : " + value;
        sectBox.appendChild(sectTitle);
        cont.appendChild(sectBox);

        const cardDeck = document.createElement("div");
        cardDeck.className = "row row-cols-1 row-cols-md-2 g-5";
        cont.appendChild(cardDeck);
        let url = 'http://127.0.0.1:9003/movie/search/' + value;
        let i = 0;
        fetch(url)
            .then(response => response.json())
            .then(result => {
                while (i < result.length) {
                    let col = document.createElement('div');
                    col.className = 'col';

                    let card = document.createElement('div');
                    card.className = 'card h-100';
                    card.style.maxWidth = '540px';

                    let row = document.createElement('div');
                    row.className = 'row g-0';

                    let cardImg = document.createElement('div');
                    cardImg.className = 'col-md-4';

                    let h5 = document.createElement('h5');
                    h5.className = "card-title";
                    let p1 = document.createElement('p');
                    p1.className = "card-text";
                    let p2 = document.createElement('p');
                    p2.className = "card-text";
                    let p3 = document.createElement('p');
                    p3.className = "card-text";
                    let p4 = document.createElement('p');
                    p4.className = "card-text";
                    let p5 = document.createElement('p');
                    p5.className = "card-text";
                    let img = document.createElement('img');
                    let a = document.createElement('a');

                    let cardCont = document.createElement('div')
                    cardCont.className = 'col-md-8';

                    let cardBody = document.createElement('div');
                    cardBody.className = 'card-body';

                    let movTitle = result[i].title;
                    img.src = result[i].poster;
                    img.alt = movTitle;
                    img.title = movTitle;
                    img.className = 'card-img';
                    cardImg.appendChild(img);

                    a.href = '/movie/' + movTitle;
                    a.appendChild(h5);
                    h5.innerHTML = movTitle;
                    cardBody.appendChild(a);

                    let movDesc = result[i].description;
                    p1.innerHTML = movDesc;
                    cardBody.appendChild(p1);

                    let movDirect = result[i].directed_by;
                    p2.innerHTML = 'Director : '.bold() + movDirect;
                    cardBody.appendChild(p2);

                    let movGenre = result[i].genre;
                    p3.innerHTML = 'Genre : '.bold() + movGenre;
                    cardBody.appendChild(p3);

                    let movYear = result[i].release_year;
                    p4.innerHTML = 'Release Year : '.bold() + movYear;
                    cardBody.appendChild(p4);

                    let movDur = result[i].duration_mins;
                    p5.innerHTML = 'Duration : '.bold() + movDur + ' mins';
                    cardBody.appendChild(p5);

                    a.addEventListener('click', function (event) {
                        event.preventDefault();
                        searchMoviebyTitle(movTitle);
                        createBackButton(getMovie)
                    })
                    cardCont.appendChild(cardBody);
                    row.appendChild(cardImg);
                    row.appendChild(cardCont);
                    card.appendChild(row);
                    col.appendChild(card);
                    cardDeck.appendChild(col);
                    i++;
                }
            });
    }

    var slideIndex = 1;

    function plusSlides(n) {
        showSlides(slideIndex += n);
    }
    document.getElementsByClassName("prev")[0].addEventListener('click', function (event) {
        event.preventDefault();
        plusSlides(-1);
    });
    document.getElementsByClassName("next")[0].addEventListener('click', function (event) {
        event.preventDefault();
        plusSlides(1);
    });

    function currentSlide(n) {
        showSlides(slideIndex = n);
    }

    async function showSlides(n) {
        var i;
        var slides = document.getElementsByClassName("mySlides");
        var dots = document.getElementsByClassName("dot");
        if (n > slides.length) { slideIndex = 1 }
        if (n < 1) { slideIndex = slides.length }
        for (i = 0; i < slides.length; i++) {
            slides[i].style.display = "none";
        }
        for (i = 0; i < dots.length; i++) {
            dots[i].className = dots[i].className.replace(" active", "");
        }
        slides[slideIndex - 1].style.display = "block";
        dots[slideIndex - 1].className += " active";
    }

    function linkToggleActive(link) {
        let linkList = document.getElementsByClassName("nav-link");
        for (let i = 0; i < linkList.length; i++) {
            linkList[i].classList.remove("active");
        }
        link.classList.add("active");
    }

    function showSnackBar(msg) {
        let snackBar = document.getElementById('snackbar');

        snackBar.innerHTML = msg;
        snackBar.className = 'show';
        setTimeout(function () {
            snackBar.className = '';
            snackBar.innerHTML = '';
        }, 3000);
    }

    const modal = document.getElementById('soloModal');
    const login = document.getElementById('lgLink');
    const modalClose = document.getElementById("modalClose");
    const modalBody = document.getElementById("modalBody");
    const modalCancel = document.getElementById("modalCancel");
    const modalTitle = document.getElementById('soloModalLabel');
    const modalFooter = document.getElementById('mdFooter');
    login.addEventListener('click', function (event) {
        event.preventDefault();
        event.stopPropagation();
        userLogin();
    })
    const alertNewAcc = document.getElementById('alertNewAcc');
    alertNewAcc.addEventListener('click', function (event) {
        event.preventDefault();
        event.stopPropagation();
        createAcc();
    })

    function createAcc() {
        modal.style.display = "block";
        modalBody.innerHTML = '';
        modalTitle.innerHTML = 'Create Account';
        let newForm = document.createElement('form');
        let newEmail = $('<div class="form-floating mb-3">\
                                    <input type="email" class="form-control" id="floatingEmail" name="email" required>\
                                    <label for="floatingEmail">Email address</label>\
                                </div>');
        let newPassword = $('<div class="form-floating mb-3">\
                                    <input type="password" class="form-control" id="floatingPassword" name="password" required>\
                                    <label for="floatingPassword">Password</label>\
                                </div>');
        let newRepPassword = $('<div class="form-floating mb-3">\
                                <input type="password" class="form-control" id="floatingRepPassword" name="repPassword" required>\
                                <label for="floatingRepPassword">Repeat Password</label>\
                            </div>');
        let newFname = $('<div class="form-floating mb-3">\
                                <input type="text" class="form-control" id="floatingFname" name="f_name" required>\
                                <label for="floatingFname">First Name</label>\
                            </div>');
        let newLname = $('<div class="form-floating mb-3">\
                            <input type="text" class="form-control" id="floatingLname" name="l_name">\
                            <label for="floatingLname">Last Name</label>\
                        </div>');
        let newAddress = $('<div class="form-floating mb-3">\
                        <input type="text" class="form-control" id="floatingAddress" name="address">\
                        <label for="floatingAddress">Address</label>\
                    </div>');
        let submitCreate = $('<button class="btn btn-primary"type="submit" id="submitCreate">Create Account</button>');
        $(newForm).append(newEmail, newPassword, newRepPassword, newFname, newLname, newAddress, submitCreate);
        $('#modalBody').append(newForm);
        $(submitCreate).click(function (event) {
            event.preventDefault();
            fetch('http://127.0.0.1:5050/newuser', {
                method: 'POST',
                body: new FormData(newForm)
            })
                .then(response => response.json())
                .then(result => {
                    if (result.alert == 'Re-enter Password does not match the Password') {
                        showSnackBar(result.alert);
                    } else if (result.alert == 'Email address has been used for another account') {
                        showSnackBar(result.alert);
                    } else if (result.alert == 'ok') {
                        document.cookie = `token=${result.token}`;
                        window.location.href = '/dashboard';
                    }
                })
        })
    }

    function userLogin() {
        modal.style.display = "block";
        modalBody.innerHTML = '';
        modalTitle.innerHTML = 'Login';
        let loginForm = document.createElement('form');
        let loginEmail = $('<div class="form-floating mb-3">\
                                <input type="email" class="form-control" id="floatingEmail" name="email" required>\
                                <label for="floatingEmail">Email address</label>\
                            </div>');
        let loginPassword = $('<div class="form-floating mb-3">\
                                    <input type="password" class="form-control" id="floatingPassword" name="password" required>\
                                    <label for="floatingPassword">Password</label>\
                                </div>');
        let submitLogin = $('<button class="btn btn-primary"type="submit" id="submitLogin">Login</button>');
        let newAcc = $("<a href='#'>Don't have an account?</a>");
        $(loginForm).append(loginEmail, loginPassword, submitLogin);
        $('#modalBody').append(loginForm, newAcc);

        $(newAcc).click(function (event) {
            event.preventDefault();
            event.stopPropagation();
            createAcc();
        })

        $(submitLogin).click(function (event) {
            event.preventDefault();
            event.stopPropagation();
            fetch('http://127.0.0.1:5050/login', {
                method: 'POST',
                body: new FormData(loginForm)
            })
                .then(response => response.json())
                .then(result => {
                    if (result.alert == "Email and Password does not match") {
                        showSnackBar("Email and Password does not match");
                    } else if (result.alert == "Email not found") {
                        showSnackBar(`${result.email} Email address not found`)
                    } else if (result.alert == 'ok') {
                        document.cookie = `token=${result.token}`;
                        window.location.href = '/dashboard';
                    } else if (result.alert == 'admin') {
                        document.cookie = 'token=admin123';
                        window.location.href = '/admin'
                    }
                })
        });
    }

    modalClose.onclick = function () {
        modal.style.display = "none";
        modalBody.innerHTML = '';
    }

    modalCancel.onclick = function () {
        modal.style.display = "none";
        modalBody.innerHTML = '';
    }

    window.onclick = function (event) {
        if (event.target == modal) {
            modal.style.display = "none";
            modalBody.innerHTML = '';
        }
    }

    if (currentUrl == defaultUrl + '/') {
        initHome();
    }

    function initHome() {
        linkToggleActive(wiLink);
        getMovie();
        history.pushState({}, null, '/');

        let sectBox1 = document.createElement("div");
        sectBox1.className = "initBox";
        let sectTitle1 = document.createElement("h3");
        sectTitle1.innerHTML = "Upcoming Movies";
        sectBox1.appendChild(sectTitle1);
        cont.appendChild(sectBox1);

        let upcomingDiv = document.createElement("div");
        let upcomingBtn = document.createElement("button");
        upcomingBtn.type = "button";
        upcomingBtn.className = "initBtn";
        upcomingBtn.innerHTML = "Browse our upcoming movie list";
        upcomingDiv.appendChild(upcomingBtn);
        cont.appendChild(upcomingDiv);
        upcomingBtn.addEventListener('click', function (event) {
            event.stopPropagation();
            event.preventDefault();
            comingsoon.click();
        })

        let sectBox2 = document.createElement("div");
        sectBox2.className = "initBox";
        let sectTitle2 = document.createElement("h3");
        sectTitle2.innerHTML = "Most Watched Movies";
        sectBox2.appendChild(sectTitle2);
        cont.appendChild(sectBox2);

        let mostwatchedDiv = document.createElement("div");
        let mostwatchedBtn = document.createElement("button");
        mostwatchedBtn.type = "button";
        mostwatchedBtn.className = "initBtn";
        mostwatchedBtn.innerHTML = "Browse our most watched movie list";
        mostwatchedDiv.appendChild(mostwatchedBtn);
        cont.appendChild(mostwatchedDiv);
        mostwatchedBtn.addEventListener('click', function (event) {
            event.stopPropagation();
            event.preventDefault();
            mostwatched.click();
        })
    }
})
from flask import Flask, jsonify, request, redirect, url_for, render_template, send_file, make_response
from flask_cors import CORS
from sqlalchemy import create_engine
from sqlalchemy.sql import text
from datetime import datetime
import requests
import decimal
import string
import random
import hashlib

app = Flask(__name__)
CORS(app)

@app.route('/img/<file>')
def get_banner(file):
    f = file
    mimetype = 'image/png'
    return send_file(f".\img\{f}", mimetype=mimetype)

@app.route('/js/<file>')
def get_file(file):
    f = file
    mimetype = "application/javascript"
    return send_file(f".\js\{f}", mimetype=mimetype)

@app.route('/css/<file>')
def get_style(file):
    f = file
    return send_file(f".\css\{f}")

@app.route('/')
def defaultUrl():
    return render_template('home_page.html')

@app.route('/nowplaying', methods=['GET'])
def indexNow_playing():
    return render_template('home_page.html')

@app.route('/movie/<title>', methods=['GET', 'POST'])
def indexDetails(title):
    return render_template('home_page.html', hidden=title)

@app.route('/movie/search', methods=['GET', 'POST'])
def indexSearch():
    title = request.args.get('q')
    return render_template('home_page.html', hidden=title)
    
@app.route('/movie/comingsoon')
def indexUpcoming():
    return render_template('home_page.html')

@app.route('/movie/mostwatched')
def indexMost_watched():
    return render_template('home_page.html')

@app.route('/login', methods=['POST'])
def loginUser():
    body = request.form
    _email = body['email']
    url = 'http://127.0.0.1:9003/login'
    x = requests.post(url, data=body)
    response = x.json()
    if response['alert'] == "Email and Password does not match":
        return jsonify(alert="Email and Password does not match")
    elif response['alert'] == "email not found":
        return jsonify(alert="Email not found", email=_email)
    elif response['alert'] == 'ok':
        return jsonify(alert='ok', token=response['token'])
    elif response['alert'] == 'admin':
        return jsonify(alert='admin')

@app.route('/newuser', methods=['POST'])
def newUser():
    body = request.form
    url = 'http://127.0.0.1:9003/new/user'
    x = requests.post(url, data=body)
    response = x.json()
    if response['alert'] == 'Re-enter Password does not match the Password':
        return jsonify(alert='Re-enter Password does not match the Password')
    elif response['alert'] == 'Email address has been used for another account':
        return jsonify(alert='Email address has been used for another account')
    elif response['alert'] == 'ok':
        return jsonify(alert='ok', token=response['token'])

@app.route('/logout')
def logoutUser():
    token = request.cookies.get('token')
    url = f'http://127.0.0.1:9003/logout/{token}'
    x = requests.get(url)
    response = x.json()
    alert = response['alert']
    if alert == 'logged out':
        res = make_response(redirect(url_for('defaultUrl')))
        res.set_cookie("token", "")
        return res
    else:
        return 'something went wrong!'

@app.route('/dashboard')
def userDash():
    token = request.cookies.get('token')
    url = f'http://127.0.0.1:9003/dashboard/{token}'
    x = requests.get(url)
    response = x.json()
    alert = response['alert']
    balance = f"{response['balance']:,}".replace(',','.')
    if alert == 'ok':
        return render_template('user_dash.html', f_name=response['f_name'], user_id=response['user_id'], balance=balance)
    elif alert == "Please create an account first":
        return redirect(url_for('defaultUrl'))

@app.route('/dashboard/nowplaying')
def user_nowplaying():
    token = request.cookies.get('token')
    url = f'http://127.0.0.1:9003/dashboard/{token}'
    x = requests.get(url)
    response = x.json()
    alert = response['alert']
    if alert == 'ok':
        return render_template('user_dash.html', f_name=response['f_name'], user_id=response['user_id'], balance=response['balance'])
    elif alert == "Please create an account first":
        return redirect(url_for('defaultUrl'))

@app.route('/dashboard/comingsoon')
def user_comingsoon():
    token = request.cookies.get('token')
    url = f'http://127.0.0.1:9003/dashboard/{token}'
    x = requests.get(url)
    response = x.json()
    alert = response['alert']
    if alert == 'ok':
        return render_template('user_dash.html', f_name=response['f_name'], user_id=response['user_id'], balance=response['balance'])
    elif alert == "Please create an account first":
        return redirect(url_for('defaultUrl'))

@app.route('/dashboard/mostwatched')
def user_mostwatched():
    token = request.cookies.get('token')
    url = f'http://127.0.0.1:9003/dashboard/{token}'
    x = requests.get(url)
    response = x.json()
    alert = response['alert']
    if alert == 'ok':
        return render_template('user_dash.html', f_name=response['f_name'], user_id=response['user_id'], balance=response['balance'])
    elif alert == "Please create an account first":
        return redirect(url_for('defaultUrl'))

@app.route('/dashboard/search')
def user_searchApp():
    title = request.args.get('q')
    token = request.cookies.get('token')
    url = f'http://127.0.0.1:9003/dashboard/{token}'
    x = requests.get(url)
    response = x.json()
    alert = response['alert']
    if alert == 'ok':
        return render_template('user_dash.html', f_name=response['f_name'], user_id=response['user_id'], balance=response['balance'], title=title)
    elif alert == "Please create an account first":
        return redirect(url_for('defaultUrl'))

@app.route('/dashboard/purchase/<schedule_id>', methods=['GET', 'POST'])
def confirmPur(schedule_id):
    token = request.cookies.get('token')
    url = f'http://127.0.0.1:9003/dashboard/{token}'
    x = requests.get(url)
    valid = x.json()

    if request.method == 'POST':
        body = request.form
        recheck = f'http://127.0.0.1:9003/seat/{schedule_id}'
        runRe = requests.get(recheck)
        recheckRes = runRe.json()
        taken = recheckRes['seat_taken']

        check = [i for i in body if i not in taken]
        if check == []:
            return jsonify(alert='seat taken')
        else:
            url = f'http://127.0.0.1:9003//purchase/{schedule_id}/{token}'
            runUrl = requests.post(url, data=body)
            response = runUrl.json()
            alert = response['alert']
            if alert == 'insuff bal':
                return jsonify(alert='insuff bal')
            if alert == 'tix purchased':
                return jsonify(alert='ok', curBal=response['curBal'])
 
    else:
        return render_template('user_dash.html', user_id=valid['user_id'], balance=valid['balance'], f_name=valid['f_name'], title=schedule_id)

@app.route('/profile/<user_id>', methods=['GET'])
def user_profile(user_id):
    token = request.cookies.get('token')
    url = f'http://127.0.0.1:9003/dashboard/{token}'
    x = requests.get(url)
    response = x.json()
    alert = response['alert']
    if alert == 'ok':
        return render_template('user_dash.html', f_name=response['f_name'], user_id=response['user_id'], balance=response['balance'])
    elif alert == "Please create an account first":
        return redirect(url_for('defaultUrl'))

@app.route('/profile/<user_id>/topup', methods=['POST', 'GET'])
def user_topup(user_id):
    if request.method == 'GET':
        return redirect(url_for('userDash'))
    else:
        token = request.cookies.get('token')
        url = f'http://127.0.0.1:9003/dashboard/{token}'
        x = requests.get(url)
        response = x.json()
        alert = response['alert']
        if alert == 'ok':
            body = request.form
            topupUrl = f'http://127.0.0.1:9003/profile/{user_id}/topup'
            topup = requests.post(topupUrl, data=body)
            res = topup.json()
            if res['msg'] == 'topup successful':
                return jsonify(alert='ok')
            else:
                return jsonify(alert='Top Up Failed')

        elif alert == "Please create an account first":
            return redirect(url_for('defaultUrl'))   

@app.route('/profile/<user_id>/edit', methods=['GET', 'POST'])
def user_edit(user_id):
    if request.method == 'GET':
        return redirect(url_for('user_profile', user_id=user_id))
    else:
        token = request.cookies.get('token')
        url = f'http://127.0.0.1:9003/dashboard/{token}'
        x = requests.get(url)
        response = x.json()
        alert = response['alert']
        if alert == 'ok':
            body = request.form
            editUrl = f'http://127.0.0.1:9003//profile/{user_id}/edit'
            edit = requests.post(editUrl, data=body)
            res = edit.json()
            if res['msg'] == 'profile updated':
                return jsonify(alert='profile updated')
            else:
                return jsonify(alert='update failed')

        elif alert == "Please create an account first":
            return redirect(url_for('defaultUrl')) 

@app.route('/profile/<user_id>/tix')
def userTix(user_id):
    token = request.cookies.get('token')
    url = f'http://127.0.0.1:9003/dashboard/{token}'
    x = requests.get(url)
    response = x.json()
    alert = response['alert']
    if alert == 'ok':
        y = requests.get(f"http://127.0.0.1:9003/user/{user_id}/tix")
        return jsonify(y.json())
    elif alert == "Please create an account first":
        return jsonify(alert='failed')

@app.route('/profile/<user_id>/trx')
def userTrx(user_id):
    token = request.cookies.get('token')
    url = f'http://127.0.0.1:9003/dashboard/{token}'
    x = requests.get(url)
    response = x.json()
    alert = response['alert']
    if alert == 'ok':
        y = requests.get(f'http://127.0.0.1:9003/user/{user_id}/trx')
        return jsonify(y.json())
    elif alert == "Please create an account first":
        return jsonify(alert="failed")

@app.route('/admin')
def adminDash():
    token = request.cookies.get('token')
    if token == 'admin123':
        return render_template('admin_dash.html')
    else:
        redirect(url_for('defaultUrl'))

@app.route('/admin/movie/edit', methods=['POST'])
def adminMovEd():
    body = request.form
    movie_id = body['movie_id']
    url = f'http://127.0.0.1:9003/admin/movie/{movie_id}'
    req = requests.post(url, data=body)
    res = req.json()
    if res['alert'] == 'movie updated':
        return jsonify(alert='success')
    else:
        return jsonify(alert='failed')

@app.route('/admin/movie/add', methods=['POST'])
def adminMovAdd():
    body = request.form
    url = f'http://127.0.0.1:9003/admin/movie/add'
    req = requests.post(url, data=body)
    res = req.json()
    if res['alert'] == 'success':
        return jsonify(alert='success')
    else:
        return jsonify(alert='failed')

@app.route('/admin/genre/edit', methods=['POST'])
def adminGenEdit():
    body = request.form
    category_id = body['category_id']
    url = f'http://127.0.0.1:9003/admin/genre/{category_id}'
    req = requests.post(url, data=body)
    res = req.json()
    if res['alert'] == 'genre updated':
        return jsonify(alert='success')
    else:
        return jsonify(alert='failed')

@app.route('/admin/genre/add', methods=['POST'])
def adminGenAdd():
    body = request.form
    url = f'http://127.0.0.1:9003/admin/genre/add'
    req = requests.post(url, data=body)
    res = req.json()
    if res['alert'] == 'genre added':
        return jsonify(alert='success')
    else:
        return jsonify(alert='failed')

@app.route('/admin/schedule/edit', methods=['POST'])
def adminSchedEdit():
    body = request.form
    schedule_id = body['schedule_id']
    url = f'http://127.0.0.1:9003/admin/schedule/{schedule_id}'
    req = requests.post(url, data=body)
    res = req.json()
    if res['alert'] == 'schedule updated':
        return jsonify(alert='success')
    else:
        return jsonify(alert='failed')

@app.route('/admin/schedule/add', methods=['POST'])
def adminSchedAdd():
    body = request.form
    url = f'http://127.0.0.1:9003/admin/schedule/add'
    req = requests.post(url, data=body)
    res = req.json()
    if res['alert'] == 'schedule added':
        return jsonify(alert='success')
    else:
        return jsonify(alert='failed')
    return 'ok'

@app.route('/admin/studio/edit', methods=['POST'])
def adminStudioEdit():
    body = request.form
    studio_id = body['studio_id']
    url = f'http://127.0.0.1:9003/admin/studio/{studio_id}'
    req = requests.post(url, data=body)
    res = req.json()
    if res['alert'] == 'studio updated':
        return jsonify(alert='success')
    else:
        return jsonify(alert='failed')

@app.route('/admin/studio/add', methods=['POST'])
def adminStudioAdd():
    body = request.form
    url = f'http://127.0.0.1:9003/admin/studio/add'
    req = requests.post(url, data=body)
    res = req.json()
 
    if res['alert'] == 'studio added':
        return jsonify(alert='success')
    else:
        return jsonify(alert='failed')

@app.route('/admin/day/edit', methods=['POST'])
def adminDayEdit():
    body = request.form
    day_id = body['day_id']
    url = f'http://127.0.0.1:9003/admin/day/{day_id}'
    req = requests.post(url, data=body)
    res = req.json()
 
    if res['alert'] == 'day updated':
        return jsonify(alert='success')
    else:
        return jsonify(alert='failed')

if __name__ == '__main__':
    app.run(port=5050)
